#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <limits.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>

#define _XOPEN_SOURCE 700
#define _GNU_SOURCE

int main(int argc, char* argv[]) {

    const int filename_len = 100;
    char* arg = getenv("PATH_TO_BROWSE");

//    Additional options for program
    bool opt_v = false;
    bool opt_w = false;
    bool ext = false;
    const int seconds = 10;

    const char* fext = getenv("EXT_TO_BROWSE");
    if(fext) {
        ext = true;
    }

    if(argc == 2) {
        if(!strcmp(argv[1],"-v")) {
            opt_v = true;
        }
        else if(!strcmp(argv[1], "-w")) {
            opt_w = true;
        }
        else if(!strcmp(argv[1],"-wv") || !strcmp(argv[1],"-vw")) {
            opt_v = true;
            opt_w = true;
        }
    }

    if(arg == NULL) {
        fprintf(stderr, "NOTE: Using current directory as PATH_TO_BROWSE\n");
        arg = malloc(sizeof(char) * filename_len);
        if (arg == NULL) {
            fprintf(stderr, "bad_alloc in file: %s line: %d\n", __FILE__, __LINE__);
            exit(1);
        }
        arg[0] = '.';
        arg[1] = '/';
        arg[2] = '\0';
    }

    //arg="tmp";
    int dfd = open(arg,O_RDONLY);
    DIR* dir = opendir(arg);

    if(dir==NULL || dfd==-1) {
        fprintf(stderr, "Failed to open file %s\n", arg);
        exit(1);
    }

    struct dirent* ptr = readdir(dir);

   // chdir(arg);
    struct stat ss;

    int child = 0;
    int own = 0;

    while(ptr) {

        int a = fstatat(dfd, ptr->d_name, &ss, 0);
        if(a == -1) {
            perror("stat");
            exit(-1);
        }



        if(strcmp(ptr->d_name,".") && strcmp(ptr->d_name, "..")) {

            if (S_ISDIR(ss.st_mode)) {

                pid_t pid = fork();
                if (pid == 0) {

                    int ret;

                    if(ext) {
                        //                    Array of string for environment variables
                        char *env[3];
                        char path[filename_len];
                        char ext_str[filename_len];

                        //PATH_TO_BROWSE=arg+"/"+ptr->d_name
                        strcpy(path, "PATH_TO_BROWSE=");
                        strcat(path, arg);
                        strcat(path, "/");
                        strcat(path, ptr->d_name);

                        strcat(ext_str, "EXT_TO_BROWSE=");
                        strcat(ext_str, getenv("EXT_TO_BROWSE"));

                        env[0] = path;
                        env[1] = ext_str;
                        env[2] = NULL;

                        if (opt_v && opt_w) {
                            ret = execle(argv[0], argv[0], "-vw", NULL, env);
                        }
                        else if (opt_v) {
                            ret = execle(argv[0], argv[0], "-v", NULL, env);
                        }
                        else if (opt_w) {
                            ret = execle(argv[0], argv[0], "-w", NULL, env);
                        }
                        else {
                            ret = execle(argv[0], argv[0], NULL, env);
                        }
                    }
                    else {
//                    Array of string for environment variables
                        char *env[2];
                        char path[filename_len];


                        //PATH_TO_BROWSE=arg+"/"+ptr->d_name
                        strcpy(path, "PATH_TO_BROWSE=");
                        strcat(path, arg);
                        strcat(path, "/");
                        strcat(path, ptr->d_name);



                        env[0] = path;
                        env[1] = NULL;

                        if (opt_v && opt_w) {
                            ret = execle(argv[0], argv[0], "-vw", NULL, env);
                        }
                        else if (opt_v) {
                            ret = execle(argv[0], argv[0], "-v", NULL, env);
                        }
                        else if (opt_w) {
                            ret = execle(argv[0], argv[0], "-w", NULL, env);
                        }
                        else {
                            ret = execle(argv[0], argv[0], NULL, env);
                        }
                    }
                    exit(ret);

                } else if (pid > 0) {
                    int tmp = 0;
					
					waitpid(pid, &tmp, 0); // Wait before sleep
					
                    if(opt_w) {
                        sleep(seconds);
                    }

                    
                    child+=WEXITSTATUS(tmp);

                }

            }
            else if(ext) {
                    int len_ext = strlen(fext);
                    int file_len = strlen(ptr->d_name);

                    --len_ext;
                    --file_len;

                    if(len_ext < file_len) {
                        do {
                            --len_ext;
                            --file_len;
                        }while(len_ext >= 0 && fext[len_ext]==ptr->d_name[file_len]);

                        if(len_ext!=-1 || ptr->d_name[file_len!='.']) {
                            goto next_iter;
                        }
                    }
                }
            ++own;
        }

        next_iter:
        ptr = readdir(dir);
    }

    closedir(dir);
    close(dfd);
//    free(arg);

    if(opt_v) {
        fprintf(stdout, "IN PROCESS %d with PATH: %s with EXT: %s\n", getpid(), getenv("PATH_TO_BROWSE"), getenv("EXT_TO_BROWSE"));
        fprintf(stdout, "Own files counter: %d\tSum: %d\n", own, child + own);
        fprintf(stdout, "\n");
    }

    return child+own;
}

/*
      If process creation failed, fork returns a value of -1 in the parent process.
      The following errno error conditions are defined for fork:

    EAGAIN

        There aren’t enough system resources to create another process,
        or the user already has too many processes running. This means exceeding the RLIMIT_NPROC resource limit,
        which can usually be increased; see Limits on Resources.
    ENOMEM

        The process requires more space than the system can supply.

 */
