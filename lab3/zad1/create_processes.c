//
// Created by ry on 3/15/16.
//





#include "create_processes.h"

static const int STACK_SIZE = 1024*1024*32;

static int clone_helper(void* arg);

void do_fork(const int max) {

    for(int i=0; i < max; ++i) {
        pid_t pid = fork();
        if(pid==0) {
            ++N;
            _exit(0);
        }
        else {
            waitpid(pid,0,0);
        }
    }

}

void do_vfork(const int max) {

    for(int i=0; i < max; ++i) {
        pid_t pid = vfork();
        if(pid == 0) {
            ++N;
            _exit(0);
        }
    }

}

void do_clone(const int max) {
	
	//void *stack = mmap(NULL,sizeof(char)*STACK_SIZE,PROT_WRITE|PROT_READ,MAP_PRIVATE|MAP_GROWSDOWN|MAP_ANONYMOUS,-1,0);

    for(int i=0; i < max ; ++i) {
		char *stack = mmap(NULL,sizeof(char)*STACK_SIZE,PROT_WRITE|PROT_READ,MAP_ANONYMOUS|MAP_STACK|MAP_SHARED,-1,0);
        pid_t pid = clone(clone_helper, stack[STACK_SIZE-1], SIGCHLD, NULL);
        waitpid(pid, 0, 0);
    }

    //free(stack);
}

void do_vclone(const int max) {

    char* stack = malloc(sizeof(char)*STACK_SIZE);

    for(int i=0; i < max ; ++i) {
        pid_t pid = clone(clone_helper, &stack[STACK_SIZE-1], CLONE_VM | CLONE_VFORK | SIGCHLD, NULL);
        waitpid(pid, 0, 0);
    }

    free(stack);
}

static int clone_helper(void* arg) {
	
	(void)arg;
    ++N;
    _exit(1);

}
