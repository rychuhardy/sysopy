#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/times.h>
#include <time.h>

#include "create_processes.h"

// Global variable counter for child processes
int N;

int save_to_file(const char* filename, double time, int iter)
{
    FILE* file = fopen(filename, "a");

    if(file==NULL) {
        return -1;
    }
    fprintf(file, "# Iter\ttime\n");
    fprintf(file, "%d\t%lf\n", iter, time);

    fclose(file);

    return 0;
}

int main(int argc, char** argv) {

    if(argc < 3) {
        fprintf(stderr, "Usage %s [number of processes] [mode]\n", argv[0]);
        return 1;
    }

    int max = atoi(argv[1]);
    if(max == 0) {
        fprintf(stderr, "Invalid number of processes %s\n", argv[1]);
        return 1;
    }


    struct tms time1,time2;
    clock_t real1, real2;

    times(&time1);
    real1 = clock();
    
    
    //RUSAGE
    struct rusage rus_m, rus_c, rus_m2, rus_c2;
	getrusage(RUSAGE_SELF, &rus_m);
	getrusage(RUSAGE_CHILDREN, &rus_c);
    //RUSAGE

    if(!strcmp("fork", argv[2])) {
        do_fork(max);
    }
    else if(!strcmp("vfork", argv[2])) {
        do_vfork(max);
    }
    else if(!strcmp("clone", argv[2])) {
        do_clone(max);
    }
    else if(!strcmp("vclone", argv[2])) {
        do_vclone(max);
    }
    else {
        fprintf(stderr, "Invalid mode %s\n", argv[2]);
        return 1;
    }

    times(&time2);
    real2 = clock();
    
    
    //RUSAGE
    getrusage(RUSAGE_SELF, &rus_m2);
    getrusage(RUSAGE_CHILDREN, &rus_c2);
    
    printf("RUSAGE: \n");
    
    double c,m;
    
    c=rus_m2.ru_stime.tv_sec + rus_m2.ru_stime.tv_usec / (double)1e6 - (rus_m.ru_stime.tv_sec + rus_m.ru_stime.tv_usec / (double)1e6);
    m=rus_c2.ru_stime.tv_sec + rus_c2.ru_stime.tv_usec / (double)1e6 -(rus_c.ru_stime.tv_sec + rus_c.ru_stime.tv_usec / (double)1e6);
    
    printf("SYS SELF %lf\n", c );
    printf("SYS CHILDREN %lf\n", m);
    printf("SYS SUM %lf\n", m+c);
    
    printf("\n\n");
    
    //RUSAGE


    long double clktck=sysconf(_SC_CLK_TCK);

    double real = (double)(real2 - real1)/CLOCKS_PER_SEC;
    double user=(time2.tms_utime-time1.tms_utime)/(double)clktck;
    double system=(time2.tms_stime-time1.tms_stime)/(double)clktck;
    double cuser=(time2.tms_cutime-time1.tms_cutime)/(double)clktck;
    double csystem=(time2.tms_cstime-time1.tms_cstime)/(double)clktck;


    printf("REAL:%lf\n",real);
    printf("USER:%lf\nSYSTEM:%lf\n",user,system);
    printf("CUSER:%lf\nCSYSTEM:%lf\n",cuser,csystem);

    printf("Counter: %d\n", N);

    const int filename_len = 20;
    char filename[filename_len];
    strcpy(filename, argv[2]);
    int len = strlen(filename);

    strcat(filename, "_sys_M_results.txt");
    save_to_file(filename, system, max);

    filename[len] = '\0';
    strcat(filename, "_sys_C_results.txt");
    save_to_file(filename, csystem, max);

    filename[len] = '\0';
    strcat(filename, "_sys_sum_results.txt");
    save_to_file(filename, system+csystem, max);


    filename[len] = '\0';
    strcat(filename, "_usr_M_results.txt");
    save_to_file(filename, user, max);

    filename[len] = '\0';
    strcat(filename, "_usr_C_results.txt");
    save_to_file(filename, cuser, max);

    filename[len] = '\0';
    strcat(filename, "_usr_sum_results.txt");
    save_to_file(filename, user+cuser, max);


    filename[len] = '\0';
    strcat(filename, "_sum_M_results.txt");
    save_to_file(filename, system+user, max);

    filename[len] = '\0';
    strcat(filename, "_sum_C_results.txt");
    save_to_file(filename, csystem+cuser, max);

    filename[len] = '\0';
    strcat(filename, "_sum_sum_results.txt");
    save_to_file(filename, system+csystem+user+cuser, max);


    filename[len] = '\0';
    strcat(filename, "_real_results.txt");
    save_to_file(filename, real, max);

    return 0;
}
