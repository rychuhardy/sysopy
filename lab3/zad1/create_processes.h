//
// Created by ry on 3/15/16.
//

#ifndef ZAD1_CREATE_PROCESSES_H
#define ZAD1_CREATE_PROCESSES_H

#define _GNU_SOURCE

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <malloc.h>
#include <sched.h>
#include <sys/mman.h>

extern int N;

void do_fork(const int max);

void do_vfork(const int max);

void do_clone(const int max);

void do_vclone(const int max);

#endif //ZAD1_CREATE_PROCESSES_H
