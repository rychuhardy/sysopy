//
// Created by ry on 4/7/16.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define BUF_SIZE 1024*1024
#define PREFIX_LEN 128
#define PID_LEN 8

int main(int argc, char* argv[]) {

    if(argc < 2) {
        fprintf(stderr, "Usage %s [pipe name]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* pipename = argv[1];

    char buf[BUF_SIZE];
    char prefix[PREFIX_LEN];
    char pid[PID_LEN];

    sprintf(pid, "%d", getpid());

    time_t tm;
    FILE* fp;

    while(fgets(buf, BUF_SIZE-PREFIX_LEN, stdin) && !ferror(stdin) && !feof(stdin)) {

        fp = fopen(pipename, "w");

        tm = time(NULL);
        strftime(prefix, sizeof(prefix), "%d/%m/%y %H:%M:%S\t", localtime(&tm));

        fputs(pid, fp);
        fputc(' ', fp);
        fputs(prefix, fp);
        fputc(' ', fp);
        fputs(buf, fp);

        fclose(fp);

    }

    return 0;
}