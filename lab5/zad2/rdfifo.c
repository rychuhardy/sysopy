#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

#define BUF_SIZE 1024*1024
#define PREFIX_LEN 128

int main(int argc, char* argv[])
{

    if(argc < 2) {
        fprintf(stderr, "Usage %s [pipe name]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* pipename = argv[1];

//    TODO: set proper privileges
    umask(0);
    if(mkfifo(pipename, S_IFIFO|0666)) {
        perror("mkfifo");
        exit(EXIT_FAILURE);
    }

    FILE* fp;

    char buf[BUF_SIZE];
    char t[PREFIX_LEN];

    time_t tm;

    while(1) {
        fp = fopen(pipename, "r");
        fgets(buf, BUF_SIZE, fp);

        tm = time(NULL);
        strftime(t, sizeof(t), "%d/%m/%y %H:%M:%S\t", localtime(&tm));

        fputs(t, stdout);
        fputs(buf, stdout);

        fclose(fp);
    }

}