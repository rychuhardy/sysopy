#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>

// Change lower to upper
// Split each word into lines so that it is not longer than N

#define BUF_SIZE 4096

void
read_from_pipe (int file, const int N)
{
    char* buf = malloc(sizeof(char)*BUF_SIZE);

    FILE *stream;
    char c;
    int it = 0;
    stream = fdopen (file, "r");
    while ((c = fgetc (stream)) != EOF)

        if(it == N) {
            buf[it] = '\0';
            fputs(buf, stdout);
            fputc('\n',stdout);
            it = 0;
            buf[it] = c;
            ++it;
        }
        else if(c == '\n') {

            buf[it] = c;
            buf[++it] = '\0';

            fputs(buf, stdout);
            it = 0;

        }
        else {
            buf[it] = c;
            ++it;
        }
//    If there is something left, print it.
    if(it) {
        buf[++it] = '\0';
        fputs(buf,stdout);
    }

    fclose (stream);
}

void
write_to_pipe (int file)
{
    FILE *stream;
    stream = fdopen (file, "w");

    //Read from stdin
    char c;
    while((c = getchar()) != EOF) {
        if(islower(c)) {
            c = toupper(c);
        }
        fprintf(stream, "%c",c);
    }

    fclose (stream);
}

int main(int argc, char* argv[]) {

    if(argc<2) {
        fprintf(stderr, "Usage %s [N]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int N = atoi(argv[1]);
    if(N < 1) {
        fprintf(stderr, "Invalid N argument: %s\n", argv[1]);
    }
    if(N > BUF_SIZE) {
        fprintf(stderr, "N argument too large: %s\n", argv[1]);
    }


    pid_t pid;
    int mypipe[2];

    /* Create the pipe. */
    if (pipe (mypipe))
    {
        fprintf (stderr, "Pipe failed.\n");
        return EXIT_FAILURE;
    }

    /* Create the child process. */
    pid = fork ();
    if (pid == (pid_t) 0)
    {
        /* This is the child process.
           Close other end first. */
        close (mypipe[1]);
        read_from_pipe (mypipe[0], N);
        return EXIT_SUCCESS;
    }
    else if (pid < (pid_t) 0)
    {
        /* The fork failed. */
        fprintf (stderr, "Fork failed.\n");
        return EXIT_FAILURE;
    }
    else
    {
        /* This is the parent process.
           Close other end first. */
        close (mypipe[0]);
        write_to_pipe (mypipe[1]);
        return EXIT_SUCCESS;
    }

}