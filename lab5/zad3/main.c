#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUF_SIZE 1024

int main(int argc, char* argv[]) {

    if(argc < 2) {
        fprintf(stderr, "Usage %s [OUTPUT]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char command[BUF_SIZE];
    sprintf(command, "grep ^d > %s", argv[1]);

    FILE* input = popen("ls -l", "r");

    FILE* output = popen(command, "w");

    if(!output || !input) {
        fprintf(stderr, "popen failed\n");
        exit(EXIT_FAILURE);
    }

    while(fgets(command, BUF_SIZE, input)) {
        fputs(command, output);
    }

    if(pclose(output)) {
        fprintf(stderr, "pclose failed\n");
        exit(EXIT_FAILURE);
    }
    if(pclose(input)) {
        fprintf(stderr, "pclose failed\n");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}