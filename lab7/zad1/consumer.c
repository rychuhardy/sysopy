//
// Created by ry on 4/21/16.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>

#define BLOCK_SIZE 1024

int* shared_data;

void sig_handler(int signum __attribute__ ((unused)))
{
    shmdt(shared_data-3);

    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    if(argc < 3 ) {
        fprintf(stderr, "Usage %s [path] [proj_id]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int proj_id = atoi(argv[2]);

    if(!proj_id) {
        fprintf(stderr, "Invalid proj_id %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "path: %s proj_id:%d\n", argv[1], proj_id);

    key_t key = ftok(argv[1], proj_id);

    int flgs = 0666;
    int shmid;
    if((shmid = shmget(key, sizeof(int) * BLOCK_SIZE, flgs)) == -1) {
        perror("shmid");
        exit(EXIT_FAILURE);
    }

    int semid;
    if((semid = semget(key, 1, 0)) == -1) {// Create only one semaphore
        perror("semget");
        exit(EXIT_FAILURE);
    }
//    Note that the nsems argument is ignored if you are explicitly opening an existing set.
    fprintf(stderr, "Attached to semaphore with id %d\n", semid);

    if(*(shared_data = shmat(shmid, 0, 0))==-1) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    const int* const arr_size = shared_data;
    ++shared_data;

    int * const insert_index = shared_data;
    ++shared_data;

    int* const remove_index = shared_data;
    ++shared_data;

    fprintf(stderr, "Attached to shared memory segment with id %d\n", shmid);

    struct sembuf sem_lock = {0,-1,0 }; // Obtain access to resources
    struct sembuf sem_unlock = {0,1,0 }; // Free access to resources

    signal(SIGINT, sig_handler);


    fprintf(stderr,"Ready to consume...\n");

    struct timeval tv;
    char time_buf[80];
    while(true) {

        if ((semop(semid, &sem_lock, 1)) == -1) {
            perror("semop");
//            exit(EXIT_FAILURE);
        }

        //Case when first task wasn't produced yet.
        if ((*remove_index) == -1) {
            if ((*insert_index) == 0) {
                goto release;
            }
            (*remove_index) = 0;
        }

        if ((*remove_index) >= (*arr_size)) {
            (*remove_index) = 0;
        }

        if ((*insert_index) == (*remove_index + 1)%(*arr_size)) { // Too much is being read.
            goto release;
        }

        int task = shared_data[(*remove_index)];
        ++(*remove_index);

        //  Number of pending tasks
        int oper_num = (*insert_index) - (*remove_index);
        if (oper_num < 0) {
            oper_num += *arr_size;
        }
        gettimeofday(&tv, NULL);
        strftime(time_buf, sizeof(time_buf), "%H:%M:%S:", localtime(&tv.tv_sec));
        printf("(%d %s%lums) Pobrałem liczbę %d z pozycji %d. Liczba zadań oczekujących: %d.\n",
               getpid(), time_buf, tv.tv_usec / 1000, task, (*remove_index) - 1, oper_num);

        release:
        if ((semop(semid, &sem_unlock, 1)) == -1) {
            perror("semop");
//            exit(EXIT_FAILURE);
        }
        sleep(rand()%3);
    }
        return 0;
}