cmake_minimum_required(VERSION 3.4)
project(zad1)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11 -Wall -pedantic -Wextra -D_GNU_SOURCE")

add_executable(cons consumer.c)
add_executable(prod producer.c)
add_executable(manager manager.c)

target_link_libraries(cons rt)
target_link_libraries(prod rt)
target_link_libraries(manager rt)
