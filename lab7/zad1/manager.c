//
// Created by ry on 4/24/16.
//

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#define BLOCK_SIZE 1024

int shmid;
int semid;

void sig_handler(int signum __attribute__ ((unused)))
{

    shmctl(shmid, IPC_RMID, 0);
    semctl(semid, IPC_RMID, 0);

    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand(time(0));

    if (argc < 3) {
        fprintf(stderr, "Usage %s [path] [proj_id]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int proj_id = atoi(argv[2]);

    if (!proj_id) {
        fprintf(stderr, "Invalid proj_id %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "path: %s proj_id:%d\n", argv[1], proj_id);

    key_t key = ftok(argv[1], proj_id);

    int flgs = 0666;
    if ((shmid = shmget(key, sizeof(int) * BLOCK_SIZE, flgs | IPC_CREAT | IPC_EXCL)) == -1) {
        perror("shmid");
        exit(EXIT_FAILURE);
    }

    int* shared_data;
    if(*(shared_data = shmat(shmid, 0, 0))==-1) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    //    First 3 elements of shared_data is size, and two indexes.
    shared_data[0] = BLOCK_SIZE - 3;
    shared_data[1] = 0;
    shared_data[2] = -1;

    fprintf(stderr, "Created shared memory segment with id %d\n", shmid);


    if((semid = semget(key, 1, flgs | IPC_CREAT | IPC_EXCL)) == -1) {// Create only one semaphore
        perror("semget");
        exit(EXIT_FAILURE);
    }

    struct sembuf sem_unlock = {0,1,0 }; // Free access to resources
    if((semop(semid, &sem_unlock, 1))==-1) {
        perror("semop");
//            exit(EXIT_FAILURE);
    }

    fprintf(stderr, "Created semaphore with id %d\n", semid);

    signal(SIGINT, sig_handler);

    while(true) {
        sleep(1);
    }
}