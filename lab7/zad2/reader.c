//
// Created by ry on 4/24/16.
//

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include <sys/time.h>


#define BLOCK_SIZE 1024

sem_t*sem_rd;
int shmfd;
int* shared_data;
const char* shm_name;

void sig_handler(int signum)
{
    //shm_unlink(shm_name);
    sem_close(sem_rd);
    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand(time(0));

    if (argc < 4) {
        fprintf(stderr, "Usage %s [reader semaphore name] [shared memory name] [value]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int val = atoi(argv[3]);
    if(val <= 0) {
        fprintf(stderr, "Invalid value argument %s\n", argv[3]);
    }

    fprintf(stderr, "reader semaphore name: %s shared memory name: %s value: %d\n", argv[1], argv[2], val);
    shm_name = argv[2];

    int flgs = 0666;
    if((sem_rd = sem_open(argv[1], 0, flgs, 1)) == SEM_FAILED)
    {
        perror("sem_open");
        exit(EXIT_FAILURE);
    };

    if ((shmfd = shm_open(argv[2], O_RDONLY, (mode_t)flgs)) == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if((shared_data = mmap(NULL, BLOCK_SIZE, PROT_READ, MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    };


    //   Array size
    const int* const arr_size = shared_data;
//    set shared data to beginning of array;
    ++shared_data;

    fprintf(stderr, "Attatched to shared memory segment with id %d\n", shmfd);

    signal(SIGINT, sig_handler);

    struct timeval tv;
    char time_buf[80];
    fprintf(stderr,"Ready to read...\n");
    while(true) {

        sem_wait(sem_rd);

        int cnt = 0;
        for(int i=0; i<*arr_size; ++i) {
            if(shared_data[i]==val) {
                ++cnt;
            }
        }

        gettimeofday(&tv, NULL);
        strftime(time_buf, sizeof(time_buf), "%H:%M:%S:", localtime(&tv.tv_sec));
        printf("(%d %s%lums) Znalazłem %d liczb o wartości %d.\n",
               getpid(), time_buf, tv.tv_usec/1000, cnt, val);

        sem_post(sem_rd);

        sleep(rand()%3);

    }
}