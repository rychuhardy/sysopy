//
// Created by ry on 4/24/16.
//

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include <sys/time.h>


#define BLOCK_SIZE 1024
#define RAND_LIMIT 32
#define MAX_READERS 8

sem_t* sem_rd;
sem_t* sem_wr;
int shmfd;
int* shared_data;
const char* shm_name;

void sig_handler(int signum)
{
    //shm_unlink(shm_name);
    sem_close(sem_rd);
    sem_close(sem_wr);
    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand(time(0));

    if (argc < 4) {
        fprintf(stderr, "Usage %s [reader semaphore name] [writer semaphore name] [shared memory name]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "reader semaphore name: %s writer semaphore name: %s shared memory name: %s\n", argv[1], argv[2], argv[3]);
    shm_name = argv[2];

    int flgs = 0666;
    if((sem_rd = sem_open(argv[1], 0, flgs, 1))==SEM_FAILED)
    {
        perror("sem_open");
        exit(EXIT_FAILURE);
    };

    if((sem_wr = sem_open(argv[2], 0, flgs, 1))==SEM_FAILED)
    {
        perror("sem_open");
        exit(EXIT_FAILURE);
    };

    if ((shmfd = shm_open(argv[3], O_RDWR, (mode_t)flgs)) == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if((shared_data = mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    };

    fprintf(stderr, "Attatched to shared memory segment with id %d\n", shmfd);


    //   Array size
    const int* const arr_size = shared_data;
    //   set shared data to beginning of array;
    ++shared_data;

    signal(SIGINT, sig_handler);

    struct timeval tv;
    char time_buf[80];
    fprintf(stderr,"Ready to write...\n");
    while(true) {

        int to_insert = rand()%(*arr_size);
        int* indexes = malloc(sizeof(int)*to_insert);

        if(indexes==NULL) {
            perror("malloc");
            continue;
        }
        for(int i=0; i<to_insert; ++i) {
            indexes[i] = rand()%(*arr_size);
        }

        //Stop writers
        sem_wait(sem_wr);

        //Stop readers
        for(int i=0; i<MAX_READERS; ++i){
            sem_wait(sem_rd);
        }

        //insert or increase numbers
        for(int i=0; i<to_insert; ++i) {
            (shared_data[indexes[i]] == -1) ? shared_data[indexes[i]] = rand()%RAND_LIMIT
                                            : (shared_data[indexes[i]] = (shared_data[indexes[i]]+1)%RAND_LIMIT);
            gettimeofday(&tv, NULL);
            strftime(time_buf, sizeof(time_buf), "%H:%M:%S:", localtime(&tv.tv_sec));
            printf("(%d %s%lums) Wpisałem liczbę %d na pozycję %d. Pozostało %d zadań.\n",
                   getpid(), time_buf, tv.tv_usec/1000, shared_data[indexes[i]], indexes[i], to_insert-i);
        }

        //unlock readers
        for(int i=0; i<MAX_READERS; ++i){
            sem_post(sem_rd);
        }
        //unlock writers
        sem_post(sem_wr);

        free(indexes);

        sleep(rand()%3);
    }
}