//
// Created by ry on 4/24/16.
//

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>


#define BLOCK_SIZE 1024
#define MAX_READERS 8

sem_t* sem_rd;
sem_t* sem_wr;
int shmfd;
int* shared_data;
const char* sem_name;
const char* sem_name2;
const char* shm_name;

void sig_handler(int signum)
{
    munmap(shared_data, BLOCK_SIZE);
    shm_unlink(shm_name);
    sem_unlink(sem_name);
    sem_unlink(sem_name2);
    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    if (argc < 4) {
        fprintf(stderr, "Usage %s [reader semaphore name] [writer semaphore name] [shared memory name]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "reader semaphore name: %s writer semaphore name: %s shared memory name: %s\n", argv[1], argv[2], argv[3]);
    sem_name = argv[1];
    sem_name2 = argv[2];
    shm_name = argv[3];

    int flgs = 0666;
    umask(0);
    if((sem_rd = sem_open(argv[1], O_CREAT | O_EXCL, flgs, MAX_READERS)) == SEM_FAILED) // MAX_READES counter in semaphore
    {
        perror("sem_open");
        exit(EXIT_FAILURE);
    };

    fprintf(stderr, "Created reader semaphore %s\n", sem_name);

    if((sem_wr = sem_open(argv[2], O_CREAT | O_EXCL, flgs, 1)) == SEM_FAILED)
    {
        perror("sem_open");
        exit(EXIT_FAILURE);
    };

    fprintf(stderr, "Created writer semaphore %s\n", sem_name2);

    if ((shmfd = shm_open(argv[3], O_CREAT | O_EXCL | O_RDWR, (mode_t)flgs)) == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if((ftruncate(shmfd, sizeof(int)*BLOCK_SIZE))==-1) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    if((shared_data = mmap(NULL, sizeof(int)*BLOCK_SIZE, PROT_WRITE | PROT_READ, MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    };
    fprintf(stderr, "Created shared memory segment with id %d\n", shmfd);

    shared_data[0] = BLOCK_SIZE - 1; // size of array

    for(int i=1; i<BLOCK_SIZE; ++i)
        shared_data[i] = -1;

    fprintf(stderr, "Finished initializing memory\n");

    signal(SIGINT, sig_handler);

    while(true) {
        sleep(10);
    }
}