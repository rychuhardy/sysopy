//
// Created by ry on 3/12/16.
//

#ifndef ZAD3_PARSER_H
#define ZAD3_PARSER_H

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

int parse_number(const char *str, unsigned *n, bool ignore_leading_spaces);
signed char parse_char(char* str, unsigned* n);

#endif //ZAD3_PARSER_H
