//
// Created by ry on 3/12/16.
//

#include "parser.h"

//Parses str looking for number, neither lead nor followed by any other character than whitespace
//Returns -1 on failure
int parse_number(const char *str, unsigned *n, bool ignore_leading_spaces) {

    if(ignore_leading_spaces) {
//    Remove any leading whitespaces
        while (str && isspace(str[*n])) {
            ++(*n);
        }
    }

//    There were only whitespaces in the str
    if(!(str+*n))
        return -1;

    int res =  atoi(str+*n);

//    Atoi returns 0 when number is not found, but 0 is also a valid result of atoi when there is '0' in the string
    if( res==0 && str[*n]!='0' ) {
        return -1;
    }

    //    Skip leading zeros
    while(str && str[*n]=='0') {
        ++(*n);
    }

//    Move to the first character behind the number
    int tmp = res;
    do {
        tmp /=10;
        ++(*n);
    }while(tmp > 0);

//    If there are no more characters or there is whitespace
    if((str+*n) || isspace(str[*n])) {
        return res;
    }

//    There was another character other than whitespace after the number
    return -1;

}


signed char parse_char(char* str, unsigned* n) {

    //    Remove any leading whitespaces
    while(str && isspace(str[*n])) {
        ++(*n);
    }

    signed char res;

//    There were only whitespaces in the str
    if(!str+*n)
        return -1;

    if(str[*n]!='\\') {
        res = str[*n];
        ++(*n);
    }
//    If this is special character
    else {
        ++(*n);
//        Case when there is only backslash and nothing after
        if(str+*n==NULL)
            return -1;

        switch(str[*n]) {

            case 'a':
                ++(*n);
                res = '\a';
                break;

            case 'b':
                ++(*n);
                res = '\b';
                break;

            case 'f':
                ++(*n);
                res = '\f';
                break;

            case 'n':
                ++(*n);
                res = '\n';
                break;

            case 'r':
                ++(*n);
                res = '\r';
                break;

            case 't':
                ++(*n);
                res = '\t';
                break;

            case 'v':
                ++(*n);
                res = '\v';
                break;

            case '\\':
                ++(*n);
                res = '\\';
                break;

            case '\'':
                ++(*n);
                res = '\'';
                break;

            case '\"':
                ++(*n);
                res = '\"';
                break;

            case '\?':
                ++(*n);
                res = '\?';
                break;
            case ' ':
                ++(*n);
                res = ' ';
                break;
            default: {
                int tmp = parse_number(str, n, false);
                if (tmp < 0 || tmp > 127) {
                    return -1;
                }
                res = tmp;
                break;
            }
        }
    }

    //    If there are no more characters or there is whitespace
    if(str[*n]==0 || isspace(str[*n])) {
        return res;
    }

//    There was another character other than whitespace after the number
    return -1;

}