#include <sys/stat.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>

#include "parser.h"

const size_t COMMAND_SIZE = 64;

bool exists(int fd, struct flock* locks, int offset, bool read, int size) 
{
	int i=0;
	while(i<size) {
		if(locks[i].l_start == offset) {
			if(read) {
				locks[i].l_type = F_RDLCK;
			}
			else {
				locks[i].l_type = F_WRLCK;
			}
			fcntl(fd, F_SETLKW, locks+i);
			return true;
		}
		++i;
	}
	return false;
}

void print(struct flock fl) {
    if(fl.l_type == F_WRLCK) {
        printf("l_type:\tWRITE\n");
    }
    else {
        printf("l_type:\tREAD \n");
    }
    printf("l_pos: \t%ld\n", fl.l_start);
    printf("l_len: \t%ld\n", fl.l_len);
    printf("l_pid: \t%d\n", fl.l_pid);

}

int lock_file(int fd,struct flock* flock, unsigned pos, bool read) {

    if(read) {
        flock->l_type = F_RDLCK;
    } else {
        flock->l_type = F_WRLCK;
    }
    flock->l_whence = SEEK_SET;
    flock->l_start=pos;
    flock->l_len = 1;
    flock->l_pid = getpid();


    if (fcntl(fd, F_SETLKW, flock) == -1) {
        perror("fcntl");
        exit(-1);
    }

    return 0;

}

int unlock_file(int fd, struct flock* flock) {

    flock->l_type = F_UNLCK;

    if (fcntl(fd, F_SETLK, flock) == -1) {
        perror("fcntl");
        exit(-1);
    }
    return 0;

}

int locks_alloc(struct flock** locks, unsigned max_size) {

    *locks = realloc(*locks, max_size);

    if(*locks) {
        return 0;
    }
    return -1;
}

//Returns non-zero when file exists.
int file_exist(char *filename) {
    struct stat buffer;
    return (stat(filename, &buffer) == 0);
}

void commands() {
    printf("\n");
    printf("Enter command number optionally followed by some arguments\n");
    printf("E.g. \"1 12\" locks 12th byte in the file for writing\n");
    printf("E.g. \"6 12 \\n\" puts newline character at given 6th position "
                   "(for special chars use escape sequences or their decimal ascii codes: t is \\116)\n");
    printf("[1] lock read [character_number] \n");
    printf("[2] lock write [character_number] \n");
    printf("[3] list \n");
    printf("[4] release [character_number] \n");
    printf("[5] read [character_number] \n");
    printf("[6] write [character_number] [new_character] \n");
    printf("\n");
}

int main(int argc, char **argv) {

    if (argc < 2) {
        fprintf(stderr, "Usage %s [file_to_lock]\n", argv[0]);
        fprintf(stderr, "Type h for help\n");
        return -1;
    }

    if(!file_exist(argv[1])) {
        fprintf(stderr, "File not found %s\n", argv[1]);
        return -1;
    }

//    Open file
    int fd;
    if((fd = open(argv[1], O_RDWR)) == -1){
        perror("open");
        exit(-1);
    }


//    Used for storing user input
    char command[COMMAND_SIZE];

    int arg1;
    signed char arg2;

//    Used for storing locks
    unsigned max_size = 10;
    struct flock* locks = NULL;
    unsigned lock_index = 0;

    int res = locks_alloc(&locks, max_size);
    if(res) {
        fprintf(stderr, "Could not allocate memory for locks\n");
    }

    bool running = true;
    while(running) {


        if(lock_index==max_size) {
            max_size*=2;
            locks_alloc(&locks, max_size);
        }

        printf("Type command (q quits the program, h displays available commands)\n");

        fgets(command, COMMAND_SIZE, stdin);
        if(command == NULL) {
            fprintf(stderr,"An error occured - cannot read command\n");
            return -1;
        }

//        Remove any leading whitespaces
        unsigned index = 0;
        while(isspace(command[index]) && index<COMMAND_SIZE) {
            ++index;
        }

//        Remove newline character
        strtok(command+index, "\n");

        switch(command[index]) {

            case 'q': case 'Q':
                running = false;
                break;
            case 'h': case 'H':
                commands();
                break;

            case '1' :
                ++index;
                arg1 = parse_number(command, &index, true);
                if(arg1 == -1) {
                    fprintf(stderr, "Error: Invalid command argument\n");
                    break;
                }
                if(!exists(fd, locks, arg1, true, lock_index)) {
					lock_file(fd, locks+lock_index, arg1, true);
					++lock_index;
				}
                break;
            case '2' :
                ++index;
                arg1 = parse_number(command, &index, true);
                if(arg1 == -1) {
                    fprintf(stderr, "Error: Invalid command argument\n");
                    break;
                }
				if(!exists(fd, locks, arg1, false, lock_index)) {
					lock_file(fd, locks+lock_index, arg1, false);
					++lock_index;
				}
                break;
            case '3' : {

                for(unsigned i=0; i<lock_index; ++i) {
                    printf("lock no %d\n", i);
                    print(locks[i]);
                    printf("\n");
                }

                int old_pos = lseek(fd, 0, SEEK_CUR);
                int last_pos=0;

                int file_size = lseek(fd, 0, SEEK_END);

                struct flock fl;

                lseek(fd, 0, SEEK_SET);
                do {

                    fl.l_type = F_WRLCK;
                    fl.l_whence = SEEK_SET;
                    fl.l_start = last_pos;
                    fl.l_len = 1;

                    if(fcntl(fd, F_GETLK, &fl)==-1)  {
                        break;
                    }
                    if(fl.l_type != F_UNLCK) {
                        print(fl);
                    }

                    last_pos = fl.l_start+fl.l_len;

                }while(last_pos < file_size);

                lseek(fd, old_pos, SEEK_SET);
//                system("lslocks | grep 'zad3'");
                break;
            }

            case '4' :
                ++index;
                arg1 = parse_number(command, &index, true);
                if( 0 > arg1 || arg1 >= (signed)lock_index) {
                    fprintf(stderr, "Error: Invalid command argument\n");
                    break;
                }
                unlock_file(fd, &locks[arg1]);


                if(arg1 != (signed)lock_index-1) {
                    memmove(&locks[arg1], &locks[lock_index - 1], sizeof(struct flock));
                }
                --lock_index;

                break;

            case '5' :
                ++index;
                arg1 = parse_number(command, &index, true);
                if(arg1 == -1) {
                    fprintf(stderr, "Error: Invalid command argument\n");
                    break;
                }
                printf("Character at position %d:",arg1);

                if(lseek(fd, arg1, SEEK_SET) < 0) {
                    fprintf(stderr, "Invalid file index\n");
                }
                read(fd, &arg2, 1);
                printf("%c\n", arg2);
                break;

            case '6' :
                //TODO: not passing second arg doesn't cause error
                ++index;
                arg1 = parse_number(command, &index, true); //doesnt parse 0 ;(
                if(arg1 == -1) {
                    fprintf(stderr, "Error: Invalid command argument\n");
                    break;
                }
                arg2 = parse_char(command, &index);
                if(arg2 == -1) {
                    fprintf(stderr, "Error: Invalid command argument\n");
                    break;
                }
                if(lseek(fd, arg1, SEEK_SET) < 0) {
                    fprintf(stderr, "Invalid file index\n");
                }
                write(fd, &arg2, 1);

                break;

            default:
                printf("Invalid argument - type h or H for help\n");
                break;
        }
    }
    free(locks);
    close(fd);
    return 0;
}
