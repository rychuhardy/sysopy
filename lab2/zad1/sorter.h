//
// Created by ry on 3/9/16.
//



#ifndef LAB2_SORTER_H
#define LAB2_SORTER_H

#include <stddef.h>
#include <stdbool.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

#include "error_types.h"

int sort(const char* filename, const ssize_t record_size, const bool is_sys);

#endif //LAB2_SORTER_H

