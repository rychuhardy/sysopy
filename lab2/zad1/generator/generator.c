//
// Created by ry on 3/10/16.
//

#include <stdio.h>
#include "generator.h"


static void generate_string(char* string, const size_t record_size);

int generate(const char *filename, const size_t record_size, const unsigned records_number) {

    FILE* fd = fopen(filename, "wb");

    if(fd == NULL) {
        return FILE_OPEN_ERROR;
    }

    srand(time(NULL));

    char string[record_size+1];
    string[record_size]='\0';

    int i = records_number;
    while(i) {
        --i;

        generate_string(string, record_size);

        fwrite(string, record_size, 1, fd);

        //fprintf(stderr, "%s\n",string);

    }

    return fclose(fd);

}

static void generate_string(char* string, const size_t record_size) {

    for(size_t i = 0; i<record_size; ++i) {
        string[i] = rand()%128;         // Safe value for both signed and unsigned char
    }


}
