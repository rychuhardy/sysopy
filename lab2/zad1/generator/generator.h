//
// Created by ry on 3/10/16.
//

#ifndef LAB2_GENERATOR_H
#define LAB2_GENERATOR_H

#include <stddef.h>
#include <stdlib.h>
#include <time.h>
#include "../error_types.h"

int generate(const char* filename, const size_t record_size, const unsigned records_number);

#endif //LAB2_GENERATOR_H
