//
// Created by ry on 3/10/16.
//

#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include "generator.h"


//Returns non-zero when file exists.
int file_exist(char *filename)
{
    struct stat buffer;
    return (stat (filename, &buffer) == 0);
}

int main(int argc, char** argv) {

    if(argc < 4) {
        fprintf(stderr, "Usage %s [filename] [record_length(in bytes)] [records_number]\n", argv[0]);
        return -1;
    }


//    First argument check
//    if(file_exist(argv[1])) {
//        fprintf(stderr, "File already exists %s\n", argv[1]);
//        return -1;
//    }

    //    Second argument check
    size_t record_size = 0;
    record_size = atoi(argv[2]);
    if(record_size < 1) {
        fprintf(stderr, "Negative record size value %s\n", argv[2]);
        return -1;
    }

    int records_number = 0;
    records_number = atoi(argv[3]);
    if(record_size < 1) {
        fprintf(stderr, "Negative records number value %s\n", argv[3]);
        return -1;
    }

    printf("filename %s size %zu number %d\n", argv[1], record_size, records_number);


    generate(argv[1], record_size, records_number);



    return 0;
}