//
// Created by ry on 3/14/16.
//

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <stdio.h>
#include <time.h>
#include <sys/resource.h>


void breakpoint(double* usr, double* sys) {


    struct rusage rus;

    getrusage(RUSAGE_SELF, &rus);

    *usr = rus.ru_utime.tv_sec + rus.ru_utime.tv_usec / (double)10e5;
    *sys = rus.ru_stime.tv_sec + rus.ru_stime.tv_usec / (double)10e5;
}

int main(int argc, char** argv) {

    if(argc < 2) {
        fprintf(stderr, "Usage: %s [filename]\n", argv[0]);
    }

    const int record_sizes_len = 4;
    const int record_numbers_len = 4;
    const int buf_len = 10; //not shorter than any of the numbers in tables

    int record_sizes[] = {4, 512, 4096, 8192};
    int record_numbers[] = {100, 500, 650, 800};


//    Time variables
    //double usr=0, sys=0, begin=0, end=0;

    //TODO: open file results.txt
    FILE* file = fopen("results.txt", "a");

    for(int i=0; i<record_sizes_len; ++i) {
        for(int j=0; j<record_numbers_len; ++j) {

            char sizes_buf[buf_len];
            char numbers_buf[buf_len];

            sprintf(sizes_buf, "%d", record_sizes[i]);
            sprintf(numbers_buf, "%d", record_numbers[j]);

            /*Spawn a child to run the program.*/
            pid_t pid=fork();
            if (pid==0) { /* child process */
                char *argv2[]={"../generator/generator", argv[1], sizes_buf, numbers_buf, NULL};
                execv("../generator/generator", argv2);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }

            //create backup of generated file
            pid = fork();
            if (pid==0) { /* child process */
                char *argv2[]={"/usr/bin/cp", argv[1], "backup.txt", NULL};
                execv("/usr/bin/cp", argv2);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }

//            start timer
//            fprintf(stdout, "Running lib sort with record size %d record number %d\n", record_sizes[i], record_numbers[j]);
            //breakpoint(&usr, &sys);
            //begin = clock();
            //double usr_prev, sys_prev;
            //usr_prev = usr;
            //sys_prev = sys;


            fseek(file, 0, SEEK_END);
            fprintf(file, "Sorting with lib functions:\n");
            fprintf(file, "Record size: %d\tRecords number: %d\n", record_sizes[i], record_numbers[j]);
            fflush(file);
            //run lib sort with original file
            pid = fork();
            if (pid==0) { /* child process */
                char *argv2[]={"/usr/bin/time", "-p", "-o", "results.txt", "--append", "../zad1", argv[1], sizes_buf, "lib", NULL};

                execv("/usr/bin/time", argv2);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */


            }

//            stop timer
           // breakpoint(&usr, &sys);
           // end = clock();
//            fprintf(stdout, "End of lib sort with record size %d record number %d\n", record_sizes[i], record_numbers[j]);
//
////            write lib results
//            fprintf(file, "Sorting with library functions:\n");
//            fprintf(file, "Record size: %d\tRecords number: %d\n", record_sizes[i], record_numbers[j]);
//            fprintf(file, "User time %lf\tSystem time %lf\tMy time %lf\n", usr-usr_prev, sys-sys_prev, (end-begin)/CLOCKS_PER_SEC);
//            fprintf(file, "\n");

            //restore backup of generated file
            pid = fork();
            if (pid==0) { /* child process */
                char *argv2[]={"/usr/bin/cp", "backup.txt", argv[1], NULL};
                execv("/usr/bin/cp", argv2);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }


//            start other timer
//            fprintf(stdout, "Running sys sort with record size %d record number %d\n", record_sizes[i], record_numbers[j]);
            //breakpoint(&usr, &sys);
           // begin = clock();
            //usr_prev = usr;
           // sys_prev = sys;

            fseek(file, 0, SEEK_END);
            fprintf(file, "Sorting with system functions:\n");
            fprintf(file, "Record size: %d\tRecords number: %d\n", record_sizes[i], record_numbers[j]);
            fflush(file);
            //run sys sort
            pid = fork();
            if (pid==0) { /* child process */
                char *argv2[]={"/usr/bin/time", "-p", "-o", "results.txt", "--append", "../zad1", argv[1], sizes_buf, "sys", NULL};
                execv("/usr/bin/time", argv2);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */

            }

//            stop other timer
            //breakpoint(&usr, &sys);
           // end = clock();
//            fprintf(stdout, "End of sys sort with record size %d record number %d\n", record_sizes[i], record_numbers[j]);
//
////            write results to file
//            fprintf(file, "Sorting with system functions:\n");
//            fprintf(file, "Record size: %d\tRecords number: %d\n", record_sizes[i], record_numbers[j]);
//            fprintf(file, "User time %lf\tSystem time %lf\tMy time %lf\n", usr-usr_prev, sys-sys_prev, (end-begin)/CLOCKS_PER_SEC);
//            fprintf(file, "\n");

        }
    }

//    TODO:  close file
    fclose(file);


    return 0;
}
