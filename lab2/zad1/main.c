#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "sorter.h"

//Returns non-zero when file exists.
int file_exist(char *filename)
{
    struct stat buffer;
    return (stat (filename, &buffer) == 0);
}

int main(int argc, char** argv) {


    if(argc < 4) {
        fprintf(stderr, "Usage %s [file] [record_length(in bytes)] [variant(sys/lib)]\n", argv[0]);
        return -1;
    }

//    First argument check
    if(!file_exist(argv[1])) {
        fprintf(stderr, "File not found %s\n", argv[1]);
        return -1;
    }

//    Second argument check
    size_t record_size = 0;
    record_size = atoi(argv[2]);
    if(record_size < 1) {
        fprintf(stderr, "Negative record size value %s\n", argv[2]);
        return -1;
    }

//    Third argument check
    bool is_sys;
    if(strcmp(argv[3],"sys")==0) {
        is_sys = true;
    }
    else if(strcmp(argv[3],"lib")==0) {
        is_sys = false;
    }
    else {
        fprintf(stderr, "Invalid third argument %s (should be 'sys' or 'lib')\n", argv[3]);
        return -1;
    }


#ifdef DEBUG__
    printf("PROGRAM ARGUMENTS: \n%s\n%s\n%s\n\n", argv[1],argv[2],argv[3]);
#endif // DEBUG__


    return sort(argv[1], record_size, is_sys);
}
