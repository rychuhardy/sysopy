//
// Created by ry on 3/9/16.
//

//#define DEBUG__

#include "sorter.h"

static int lib_sort(const char* filename, const size_t record_size);
static int sys_sort(const char* filename, const ssize_t record_size);


int sort(const char *filename, const ssize_t record_size, const bool is_sys) {

    if(is_sys)
        return sys_sort(filename, record_size);
    else
        return lib_sort(filename, record_size);

}

static int sys_sort(const char* filename, const ssize_t record_size) {

//    TODO: Change strcmp to comparing only first bytes
    enum error_type return_value = 0;
//    File descriptor
    int fd = open(filename, O_RDWR);
    if(fd == -1) {
        return FILE_OPEN_ERROR;
    }


#ifdef DEBUG__
    printf("FILE OPENED\n");
#endif


    char first_buf[record_size+1];
    char second_buf[record_size+1];

    first_buf[record_size] = '\0';
    second_buf[record_size] = '\0';

    ssize_t first_read, second_read;


//    Start with second line
    unsigned counter = 1;
    if( lseek(fd, 1*record_size, SEEK_SET) == -1 ) {
        return_value = LSEEK_ERROR;
        goto cleanup;
    }


//    While EOF is not reached
    while((first_read = read(fd, &first_buf, record_size))) {


//    Some text editors add newline character at the end of the file
#ifdef NEWLINE__
        if (strcmp(first_buf,"\n") == 0) {
            continue;
        }
#endif // NEWLINE__


#ifdef DEBUG__
        printf("READ STRING: %s IN %d RECORD TO FIRST BUFFER\n", first_buf, counter);
#endif // DEBUG__

//        Counter stores the number of records already sorted
        ++counter;

//        Check if numbers of read bytes is equal to record_size
        if(first_read!=record_size) {
            return_value = BAD_RECORD_SIZE;
            goto cleanup;
        }

//        Go back to the value before the read record
        if( lseek(fd, -2*record_size, SEEK_CUR) == -1 ) {
            return_value = LSEEK_ERROR;
            goto cleanup;
        }

//        Read value to compare
        second_read = read(fd, &second_buf, record_size);
        if(second_read != record_size) {
            return_value = BAD_RECORD_SIZE;
            goto cleanup;
        }


#ifdef DEBUG__
        printf("\tREAD STRING %s TO SECOND BUFFER\n", second_buf);
#endif // DEBUG__


        while( first_buf[0] < second_buf[0] ) {


#ifdef DEBUG__
            int c__;
            c__ = first_buf[0] < second_buf[0];
                printf("\t\tLESS BETWEEN %s & %s RETURNED %d\n", first_buf, second_buf, c__);
#endif // DEBUG__


//        Write second_buf one line further in the file
            second_read = write (fd, &second_buf, record_size);
            if(second_read != record_size) {
                return_value = WRITE_ERROR;
                goto cleanup;
            }


#ifdef DEBUG__
            printf("\t\tWRITE STRING %s RECORD FROM SECOND BUFFER IN POSITION %d\n", second_buf, (int)lseek(fd,0,SEEK_CUR)-1);
#endif // DEBUG__



//            Move to the next record to be compared with first_buf

//            If this returns -1 it means that we are at negative offset - before the beginning of the file
            if( lseek(fd, -3*record_size, SEEK_CUR) != -1 ) {
//            Read next record to compare
                second_read = read(fd, &second_buf, record_size);
                if (second_read != record_size) {
                    return_value = BAD_RECORD_SIZE;
                    goto cleanup;
                }


#ifdef DEBUG__
                printf("\t\tREAD STRING %s RECORD TO SECOND BUFFER\n", second_buf);
#endif // DEBUG__


            }
//                The case when beginning of the file is reached.
            else {
                lseek(fd, -2*record_size, SEEK_CUR);
                break;
            }


#ifdef DEBUG__
            getchar();
#endif // DEBUG__


        } // End of inner while

//        Write first_record
        first_read = write (fd, &first_buf, record_size);
        if(first_read != record_size) {
            return_value = WRITE_ERROR;
            goto cleanup;
        }


#ifdef DEBUG__
        printf("\tWRITE STRING %s RECORD FROM FIRST BUFFER IN POSITION %d\n", first_buf, (int)lseek(fd,0,SEEK_CUR)-1);
#endif // DEBUG__


//        Move to the next record
        if( lseek(fd, counter*record_size, SEEK_SET) == -1 ) {
            return_value = LSEEK_ERROR;
            goto cleanup;
        }

    } // End of outer while

cleanup:
//    TODO: close() returns non-zero on close failure???
    close(fd);
    return return_value;

}

static int lib_sort(const char* filename, const size_t record_size) {

    int return_value = 0;

    FILE* fd = fopen(filename, "rb+");
    if(fd == NULL) {
        return FILE_OPEN_ERROR;
    }


#ifdef DEBUG__
    printf("FILE OPENED\n");
#endif


    char first_buf[record_size+1];
    char second_buf[record_size+1];

    first_buf[record_size] = '\0';
    second_buf[record_size] = '\0';

    size_t first_read, second_read;

    //    Start with second line
    unsigned counter = 1;
    if( fseek(fd, 1*record_size, SEEK_SET) != 0 ) {
        return_value = FSEEK_ERROR;
        goto cleanup;
    }

//    While EOF is not reached
    while((first_read = fread(&first_buf, record_size, 1, fd))) {


#ifdef DEBUG__
        printf("READ STRING: %s IN %d RECORD TO FIRST BUFFER\n", first_buf, counter);
#endif // DEBUG__


//        Counter stores the number of records already sorted
        ++counter;

//        Go back to the value before the read record
        if( fseek(fd, -2*record_size, SEEK_CUR) != 0 ) {
            return_value = FSEEK_ERROR;
            goto cleanup;
        }

//        Read value to compare
        second_read = fread(&second_buf, record_size, 1, fd);
        if(second_read != 1) {
            return_value = FREAD_ERROR;
            goto cleanup;
        }


#ifdef DEBUG__
        printf("\tREAD STRING %s TO SECOND BUFFER\n", second_buf);
#endif // DEBUG__


        while( first_buf[0] < second_buf[0]  ) {


#ifdef DEBUG__
            bool c__;
            if((c__ = first_buf[0] < second_buf[0]))
                printf("\t\tCOMPARE BETWEEN %s & %s RETURNED %d\n", first_buf, second_buf, c__);
#endif // DEBUG__


//        Write second_buf one line further in the file
            second_read = fwrite (&second_buf, record_size, 1, fd);
            if(second_read != 1) {
                return_value = WRITE_ERROR;
                goto cleanup;
            }


#ifdef DEBUG__
            printf("\t\tWRITE STRING %s RECORD FROM SECOND BUFFER IN POSITION %d\n", second_buf, (int)ftell(fd)-1);
#endif // DEBUG__


//            Move to the next record to be compared with first_buf
//            If this returns -1 it means that we are at negative offset - before the beginning of the file
            if(fseek(fd, -3*record_size, SEEK_CUR)==0) {
//            Read next record to compare
                second_read = fread(&second_buf, record_size, 1, fd);
                if (second_read != 1) {
                    return_value = FREAD_ERROR;
                    goto cleanup;
                }


#ifdef DEBUG__
                printf("\t\tREAD STRING %s RECORD TO SECOND BUFFER\n", second_buf);
#endif // DEBUG__
            }


            else {
                fseek(fd, 0, SEEK_SET);
//                fseek(fd,-2*record_size, SEEK_CUR)
                break;
            }


#ifdef DEBUG__
            getchar();
#endif // DEBUG__


        } // End of inner while

//        Write first_record
        first_read = fwrite (&first_buf, record_size, 1, fd);
        if(first_read != 1) {
            return_value = WRITE_ERROR;
            goto cleanup;
        }


#ifdef DEBUG__
        printf("\tWRITE STRING %s RECORD FROM FIRST BUFFER IN POSITION %d\n", first_buf, (int)ftell(fd)-1);
#endif // DEBUG__


//        Move to the next record
        if( fseek(fd, counter*record_size, SEEK_SET) != 0 ) {
            return_value = FSEEK_ERROR;
            goto cleanup;
        }

    } // End of outer while

    cleanup:
//    TODO: fclose() returns non-zero on close failure
    fclose(fd);
    return return_value;

}
