//
// Created by ry on 3/8/16.
//

#ifndef LAB2_ERROR_TYPES_H
#define LAB2_ERROR_TYPES_H

enum error_type {
    FILE_OPEN_ERROR = -1,
    LSEEK_ERROR = -2,
    BAD_RECORD_SIZE = -3,
    WRITE_ERROR = -4,
    FSEEK_ERROR = -5,
    FREAD_ERROR = -6
};


#endif //LAB2_ERROR_TYPES_H
