
#define _GNU_SOURCE

#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <math.h>
#include <ftw.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"

bool test_perm(mode_t mode, int perm);

int perm__;

int apply(const char* filepath, const struct stat* stat, int typeflag, struct FTW* ftw_buf) {

    if(!test_perm(stat->st_mode, perm__)) {
        return 0;
    }

   // char * absolute = realpath(filepath, 0);
    if(typeflag == FTW_D) {
        printf(ANSI_COLOR_RED);

    }
    else if(typeflag == FTW_F) {
        printf(ANSI_COLOR_GREEN);
    }

    printf("%*c", 4*ftw_buf->level, 0);
    printf("%s\t%ld\t%s\t\n", filepath, stat->st_size, ctime(&stat->st_atime));
    printf(ANSI_COLOR_RESET);

    return 0;

}

bool test_perm_helper(mode_t mode, int perm, int privilige) {
    if(perm & privilige) {
        if (!(mode & privilige)) {
            return false;
        }
    }
    else {
        if(mode & privilige) {
            return false;
        }
    }
    return true;
}

bool test_perm(mode_t mode, int perm) {

//      TODO: Not found S_ISVTX - sticky bit

    return     test_perm_helper(mode, perm, S_IRUSR)
            && test_perm_helper(mode, perm, S_IWUSR)
            && test_perm_helper(mode, perm, S_IXUSR)

            && test_perm_helper(mode, perm, S_IRGRP)
            && test_perm_helper(mode, perm, S_IWGRP)
            && test_perm_helper(mode, perm, S_IXGRP)

            && test_perm_helper(mode, perm, S_IROTH)
            && test_perm_helper(mode, perm, S_IWOTH)
            && test_perm_helper(mode, perm, S_IXOTH);

}

int traverse(DIR* dir, unsigned depth, const char* prefix, int perm) {

    struct dirent* ptr = readdir(dir);
    struct stat ss;
    while(ptr) {

        int a = stat(ptr->d_name, &ss);
        if(a == -1) {
            perror("stat");
            exit(-1);
        }

        if(!test_perm(ss.st_mode, perm)) {
            ptr = readdir(dir);
            continue;
        }

        if(S_ISDIR(ss.st_mode)) {

            printf("%*c", 4*depth, 0);

            printf(ANSI_COLOR_RED);
            printf("%s/%s\t%ld\t%s\t\n", prefix, ptr->d_name, ss.st_size, ctime(&ss.st_atime));
            printf(ANSI_COLOR_RESET);

            if(strcmp(ptr->d_name,  ".") && strcmp(ptr->d_name, "..")) {
                DIR *next_dir = opendir(ptr->d_name);

                //set directory
                chdir(ptr->d_name);

                char* relative = malloc(strlen(prefix)+strlen(ptr->d_name)+1+1);
                strcpy(relative, prefix);
                strcat(relative, "/");
                strcat(relative, ptr->d_name);

                traverse(next_dir, depth+1, relative, perm);

                free(relative);

                closedir(next_dir);

                //unset directory
                chdir("..");
            }

        }
        else if(S_ISREG(ss.st_mode)){
            printf("%*c", 4*depth, 0);
            printf(ANSI_COLOR_GREEN);
            printf("%s/%s\t%ld\t%s\n", prefix, ptr->d_name, ss.st_size, ctime(&ss.st_atime));
            printf(ANSI_COLOR_RESET);
        }

        errno = 0;
        ptr = readdir(dir);
        if(errno == EBADF) {
            perror("readdir");
            return -1;
        }

    }
    return 0;
}

int convert_to_dec(int val) {

    int counter = 0;
    int res = 0;

    while(val > 0) {

        if((val%10) > 8)
            return -1;
        res+=(val%10)*pow(8, counter);

        val/=10;
        ++counter;
    }
    return res;

}

int main(int argc, char** argv) {

    if(argc<3) {
        fprintf(stderr, "Usage %s [path] [priviliges]\n", argv[0]);
        return -1;
    }

   DIR* dir = opendir(argv[1]);

   if(dir == NULL) {
       perror("opendir");
       return -1;
    }

    int mode = convert_to_dec(atoi(argv[2]));
    if((mode==0 && argv[2][0]!='0') || mode < 0) {
        fprintf(stderr, "Invalid priviliges argument - use format with octal numbers, e.g. '700'\n");
        return -1;
    }
    perm__ = mode;

    chdir(argv[1]);
    traverse(dir, 0, "", mode);

    closedir(dir);


    //int max_open_dirs = 10;
    //nftw(argv[1], apply, max_open_dirs, FTW_CHDIR | FTW_MOUNT | FTW_PHYS);
    
       // char * xd = realpath(ptr->d_name, NULL);




    return 0;
}
