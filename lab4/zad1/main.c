#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>

volatile sig_atomic_t rev; //Print string reversed
volatile sig_atomic_t inc=1; //Increment or decrement repetitions
volatile sig_atomic_t repeat = 1; // Number of repetitions
volatile sig_atomic_t max_repeat; // Program's argument
volatile sig_atomic_t lock; // Simplest lock ever

void print_rev(const char* str) {

    for(size_t i = strlen(str); i>0; --i) {
        printf("%c", str[i-1]);
    }

}

void sigint_handler(int signum) {
    (void)signum;

    fprintf(stderr, "Odebrano sygnał SIGINT\n");

    signal(SIGINT, SIG_DFL);
    raise(SIGINT);
}

void sigtstp_handler(int signum) {
    (void)signum;

    while(!lock) { // Wait until loop ends
        if(!rev) { // Reverse
            rev = 1;
        }
        else if(inc) {  // If increasing the number of repetitions
            if(++repeat==max_repeat) { // Next signal shall decrease repetitions
                inc = 0;
            }
            rev =0;
        }
        else {
            if(--repeat==1) {
                inc = 1;
            }
            rev = 0;
        }
        break; // Do just one iteration per signal
    }
}

int main(int argc, char* argv[]) {

    if(argc < 3) {
        fprintf(stderr, "Usage %s [text] [max-num]\n", argv[0]);
        exit(1);
    }

    char* text = argv[1];

    int max = atoi(argv[2]);
    if(max < 1) {
        fprintf(stderr, "Invalid max-num argument: %s\n", argv[2]);
    }
    max_repeat = max;

    signal(SIGINT, sigint_handler);

    struct sigaction sig;

    sig.sa_handler = sigtstp_handler;
    sig.sa_flags = SA_RESTART;
    sigaction(SIGTSTP, &sig, NULL);

    while(true) {
        lock = 1;
        for(int i = 0; i<repeat; ++i) {
            if(rev) {
                print_rev(text);
            }
            else {
                printf("%s", text);
            }

        }
        printf("\n");
        lock = 0;
        sleep(2);
    }

    return 0;
}
