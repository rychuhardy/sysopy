//
// Created by ry on 3/27/16.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>

volatile sig_atomic_t counter;
volatile sig_atomic_t pid;
volatile sig_atomic_t usr_interrupt;



void handler_usr1(int signum __attribute__ ((unused)), siginfo_t* info __attribute__ ((unused)), void* vp __attribute__ ((unused))) {

    if(pid==0) {
        pid = info->si_pid;
    }

    if(pid==info->si_pid) {
        ++counter;
//        Modification: Confirm signal delivery
        kill(pid, SIGUSR1);
    }

}

void handler_usr2(int signum __attribute__ ((unused))) {
    usr_interrupt = 1;
//    Modification: Confirm signal delivery
    kill(pid, SIGUSR2);

}

int main() {

    fprintf(stderr, "My PID: %d\n", getpid());

    sigset_t set1, set2;
    sigfillset(&set1);
    sigfillset(&set2);

    struct sigaction sig1;
    sig1.sa_sigaction = handler_usr1;
    sig1.sa_mask = set1;
    sig1.sa_flags = SA_RESTART | SA_SIGINFO;
    sigaction(SIGUSR1, &sig1, NULL);

    struct sigaction sig2;
    sig2.sa_handler= handler_usr2;
    sig2.sa_mask = set2;
    sig2.sa_flags = SA_RESTART;
    sigaction(SIGUSR2, &sig2, NULL);


    sigset_t mask, old_mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    sigaddset(&mask, SIGUSR2);

    sigprocmask(SIG_BLOCK, &mask, &old_mask);
    while(!usr_interrupt) {
        sigsuspend(&old_mask);
    }
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    fprintf(stderr, "Received %d signal(s)\n", counter);

    return 0;
}