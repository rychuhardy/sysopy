#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

volatile sig_atomic_t usr_interrupt;
volatile sig_atomic_t counter;

void handler_usr1(int signum __attribute__ ((unused))) {
    ++counter;
}

void handler_usr2(int signum __attribute__ ((unused))) {
    usr_interrupt = 1;
}

int main(int argc, char* argv[]) {

    if(argc < 3) {
        fprintf(stderr, "Usage %s [pid] [signals_num]\n", argv[0]);
        exit(1);
    }

    int pid = atoi(argv[1]);
    if(! (pid > 0)) {
        fprintf(stderr, "Invalid pid argument %s\n", argv[1]);
        exit(1);
    }

    int signals = atoi(argv[2]);
    if( ! (signals > 0) ) {
        fprintf(stderr, "Invalid signals_num argument %s\n", argv[2]);
        exit(1);
    }

    fprintf(stderr, "My PID: %d\n", getpid());

//    Sets for sigprocmask
    sigset_t mask, old_mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    sigaddset(&mask, SIGUSR2);

//    Sets for sigaction
    sigset_t set1, set2;
    sigfillset(&set1);
    sigfillset(&set2);

    //Set handler for sigusr1
    struct sigaction sig1;
    sig1.sa_handler = handler_usr1;
    sig1.sa_mask = set1;
    sig1.sa_flags = SA_RESTART;

    //Set handler for sigusr2
    struct sigaction sig2;
    sig2.sa_handler = handler_usr2;
    sig2.sa_mask = set2;
    sig2.sa_flags = SA_RESTART;

    sigaction(SIGUSR1, &sig1, NULL);

    sigaction(SIGUSR2, &sig2, NULL);


    //Send signals
    int failed = 0;
    sigprocmask(SIG_BLOCK, &mask, &old_mask);
    for(int i=0; i < signals; ++i) {
        if( kill(pid, SIGUSR1)==-1) {
            ++failed;
        }
        else {
            sigsuspend(&old_mask);
        }

    }
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    //Send sigusr2
    if(kill(pid, SIGUSR2)==-1) {
        fprintf(stderr, "ERROR: Could not send SIGUSR2\n");
        exit(1);
    }

    fprintf(stderr, "Sent %d signal(s)\n", signals - failed);

    sigprocmask(SIG_BLOCK, &mask, &old_mask);
    while(!usr_interrupt) {
        sigsuspend(&old_mask);
    }
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    //Print results
    printf("Requested number of signal(s): %d\n", signals);
    printf("The program failed to send: %d signal(s)\n", failed);
    printf("The other program responded with %d signal(s)\n", counter);

    return 0;
}
