#include <stdlib.h>
#include <stdio.h>

#define RECORD_SIZE 1024


int main(int argc, char* argv[]){

  if(argc < 3) {
      fprintf(stderr, "Usage: %s [filename] [records number]\n", argv[0]);
      exit(EXIT_FAILURE);
  }

  int records = atoi(argv[2]);
    if(records<=0) {
        fprintf(stderr, "Invalid records argument %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    const char* filename = argv[1];

  FILE* fp =fopen(filename,"w+");
  if(!fp) {
      ferror(fp);
  }

  for(int i=0;i<records;i++){
      int written = fwrite(&i, sizeof(int), 1, fp);
      written*=sizeof(int)/sizeof(char);
        while(written < RECORD_SIZE) {
            char c = (rand()%26)+'a';
            fwrite(&c, sizeof(char), 1, fp);
            ++written;
        }
  }

  fclose(fp);
  return 0;
}
