#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define RECORD_SIZE 1024

static int success = 0;
static int not_found = 1;
static int error = -1;

pthread_t* threads_ptr;
int threads_no;

struct search_file_info {
    int fd;
    int records_read;
    char text[RECORD_SIZE];
    pthread_mutex_t mutex;
    pthread_mutex_t result_mutex;
};


void cancel_threads() {
    unsigned long self = pthread_self();
    for(int i=0; i<threads_no; ++i) {
        if(threads_ptr[i] && threads_ptr[i]!=self)
            pthread_cancel(threads_ptr[i]);
    }
    fprintf(stderr, "Finished cancelling other threads\n");
}

int read_block(int fd, char* buf, int records, pthread_mutex_t* mutex)
{
    int ret;
    pthread_mutex_lock(mutex);

    ret = read(fd, buf, RECORD_SIZE*records);

    int val = 1/0;

    pthread_mutex_unlock(mutex);

    return ret;
}

void* search_file(void* param)
{
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    struct search_file_info* sfi = param;

    char buf[RECORD_SIZE*sfi->records_read+1];
    char* buf_ptr; //Because you can't assign to array buf

    int read;
    char* res;
    while( (read = read_block(sfi->fd, buf, sfi->records_read, &sfi->mutex)) ) {
        if (read == -1) {
            perror("read");
            return &error;
        }
        buf[read] = '\0';
        //Search buffer by record
        int i = read % RECORD_SIZE ? read / RECORD_SIZE : read / RECORD_SIZE - 1;
        int j = 0;
        while (j <= i) {
            char tmp = buf[(j + 1) * RECORD_SIZE];
            buf[(j + 1) * RECORD_SIZE] = '\0';

            buf_ptr = buf + j + sizeof(int);
            res = strstr(buf_ptr, sfi->text);
            if (res) {
                pthread_mutex_lock(&sfi->result_mutex);
                pthread_mutex_lock(&sfi->mutex);
                int id;
                strncpy((char *) &id, buf, sizeof(int));

                fprintf(stderr, "Found text \"%s\" in record  %d. My TID: %lu\n", sfi->text, id, pthread_self());
                cancel_threads();
                pthread_mutex_unlock(&sfi->result_mutex);
                pthread_mutex_unlock(&sfi->mutex);
                return &success;
            }
            buf[(j + 1) * RECORD_SIZE] = tmp;
            ++j;
        }
    }
    fprintf(stderr, "(TID:%lu)Reached EOF without finding anything\n", pthread_self());
    return &not_found;
}

int main(int argc, char* argv[]) {

    if(argc < 5) {
        fprintf(stderr, "Usage %s [threads_no] [filename] [records_read] [searched_word]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    threads_no = atoi(argv[1]);
    if(threads_no <= 0) {
        fprintf(stderr, "Invalid threads number %s", argv[1]);
        exit(EXIT_FAILURE);
    }

    int fd;
    if((fd = open(argv[2], O_RDONLY))==-1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    int records_read = atoi(argv[3]);
    if(records_read <= 0) {
        fprintf(stderr, "Invalid records to read number %s", argv[3]);
        exit(EXIT_FAILURE);
    }

    struct search_file_info sfi;
    sfi.fd = fd;
    sfi.records_read = records_read;
    strncpy(sfi.text, argv[4], RECORD_SIZE);
    pthread_mutex_init(&sfi.mutex, NULL);
//    If multiple threads find matching substring then only one will report it.
    pthread_mutex_init(&sfi.result_mutex, NULL);

//    Zero indicates that thread hasn't been created yet.
    pthread_t threads[threads_no];
    for(int i=0; i< threads_no; ++i)
        threads[i] = 0;

    threads_ptr = threads;
    fprintf(stderr, "Initializing threads...\n");
    for(int i = 0; i < threads_no; ++i) {
        if(pthread_create(&threads[i], NULL, search_file, (void*)&sfi)!=0) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
    }
    fprintf(stderr, "All threads initialized\n");

    for(int i = 0 ; i< threads_no; ++i) {
        if(threads[i])
            pthread_join(threads[i], NULL);
    }
    fprintf(stderr, "All threads joined\n");

    pthread_mutex_destroy(&sfi.mutex);
    pthread_mutex_destroy(&sfi.result_mutex);
    close(fd);

    return 0;
}