#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include <signal.h>
#include <sys/syscall.h>

int K, N;

volatile int on_carrier;
pthread_mutex_t takeoff_m =PTHREAD_MUTEX_INITIALIZER, land_m=PTHREAD_MUTEX_INITIALIZER, landing=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t takeoff=PTHREAD_COND_INITIALIZER, land=PTHREAD_COND_INITIALIZER;
volatile int takeoff_cnt, landing_cnt;
bool is_free;

bool flying_cond = true;

void free_lane() {
    pthread_mutex_lock(&landing);
    
    if(on_carrier < K) {
        if(landing_cnt) {
            pthread_cond_signal(&land);
        }
        else {
            pthread_cond_signal(&takeoff);
        }
    }
    else {
        if(takeoff_cnt) {
            pthread_cond_signal(&takeoff);
        }
        else if(on_carrier < N) {
            pthread_cond_signal(&land);
        }
    }
    
    fprintf(stderr, "(%lu, %f)Zwalniam pas\n", syscall(SYS_gettid), clock()*1000000.0F/1000);
    pthread_mutex_unlock(&landing);
}

void takeoff_request() {
	//fprintf(stderr, "(%lu)Chcę startować\n", syscall(SYS_gettid));
	pthread_mutex_lock(&takeoff_m);
    if(!is_free) {
        ++takeoff_cnt;
        //added while
        while(!is_free)
			pthread_cond_wait(&takeoff, &takeoff_m);
        --takeoff_cnt;
        is_free = false;
    }
    pthread_mutex_unlock(&takeoff_m);
    fprintf(stderr, "(%lu,%f)Chcę startować\n", syscall(SYS_gettid), clock()*1000000.0F/1000);
}

void took_off() {  
	pthread_mutex_lock(&takeoff_m); 
    --on_carrier;
    is_free = true;
    fprintf(stderr, "(%lu,%f)Wystartowałem\n", syscall(SYS_gettid), clock()*1000000.0F/1000);
    free_lane();
    pthread_mutex_unlock(&takeoff_m);
}

void land_request() {
	pthread_mutex_lock(&land_m);
    //fprintf(stderr, "(%lu)Chcę lądować\n", syscall(SYS_gettid));
    //was if
    while(!is_free || on_carrier == N) {
        pthread_cond_wait(&land, &land_m);
    }
    is_free = false;
    fprintf(stderr, "(%lu, %f)Chcę lądować\n", syscall(SYS_gettid), clock()*1000000.0F/1000);
    pthread_mutex_unlock(&land_m);
}

void landed() {
	pthread_mutex_lock(&land_m);
    ++on_carrier;
    is_free = true;
    fprintf(stderr, "(%lu, %f)Wylądowałem\n", syscall(SYS_gettid), clock()*1000000.0F/1000);
    free_lane();
    pthread_mutex_unlock(&land_m);
}


void* flying(void* arg __attribute((unused)))
{
    while(flying_cond) {
        
        sleep(rand()%6);
        land_request();
        sleep(rand()%2);
        landed();
        sleep(rand()%6);
        takeoff_request();
        sleep(rand()%2);
        took_off();
    }

    return NULL;
}

void handler(int signum __attribute__((unused))) {
   flying_cond = false;
}

int main(int argc, char* argv[]) {

    srand(time(0));

    if(argc < 3) {
        fprintf(stderr, "Usage: %s [k] [n] [planes]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    K = atoi(argv[1]);
    if(K <= 0 ) {
        fprintf(stderr, "Invalid K argument: %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    N = atoi(argv[2]);
    if(N <= 0 ) {
        fprintf(stderr, "Invalid N argument: %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    int planes = atoi(argv[3]);
    if(planes <= 0 ) {
        fprintf(stderr, "Invalid planes argument: %s\n", argv[3]);
        exit(EXIT_FAILURE);
    }

    is_free = true;
    on_carrier = 0;

    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    struct sigaction sig;
    sig.sa_handler = handler;
    //sigaction(SIGINT, &sig, NULL);

    pthread_t threads[planes];
    for(int i=0; i<planes; ++i) {
        pthread_create(&threads[i], NULL, flying, NULL);
    }

    for(int i=0; i<planes; ++i) {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&landing);
    pthread_mutex_destroy(&land_m);
    pthread_mutex_destroy(&takeoff_m);
    pthread_cond_destroy(&takeoff);
    pthread_cond_destroy(&land);

    return 0;
}
