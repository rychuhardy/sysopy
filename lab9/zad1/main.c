#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
    int position;
    int count;
    sem_t *forks;
    sem_t *waiter;
} params_t;

void initialize_semaphores(sem_t *waiter, sem_t *forks, int num_forks);
void run_all_threads(pthread_t *threads, sem_t *forks, sem_t *waiter, int num_philosophers);

void *philosopher(void *params);
void think(int position);
void eat(int position);

int iter;

int main(int argc, char *argv[])
{
    if(argc < 3) {
        fprintf(stderr, "Usage %s [philosophers_num] [iter_num(0 is inifity)]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    int num_philosophers = atoi(argv[1]);
    if(num_philosophers <= 0) {
        fprintf(stderr, "Invalid philosphers number %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    iter = atoi(argv[2]);

    sem_t waiter;
    sem_t forks[num_philosophers];
    pthread_t philosophers[num_philosophers];

    initialize_semaphores(&waiter, forks, num_philosophers);
    run_all_threads(philosophers, forks, &waiter, num_philosophers);
    pthread_exit(NULL);
}

void initialize_semaphores(sem_t *waiter, sem_t *forks, int num_forks)
{
    int i;
    for(i = 0; i < num_forks; i++) {
        sem_init(&forks[i], 0, 1);
    }

    sem_init(waiter, 0, num_forks - 1);
}

void run_all_threads(pthread_t *threads, sem_t *forks, sem_t *waiter, int num_philosophers)
{
    int i;
    for(i = 0; i < num_philosophers; i++) {
        params_t *arg = malloc(sizeof(params_t));
        arg->position = i;
        arg->count = num_philosophers;
        arg->waiter = waiter;
        arg->forks = forks;

        pthread_create(&threads[i], NULL, philosopher, (void *)arg);
    }
}

void *philosopher(void *params)
{
    params_t self = *(params_t *)params;

    for(int i = 0; i < iter || !iter; i++) {
        think(self.position);

        sem_wait(self.waiter);
        sem_wait(&self.forks[self.position]);
        fprintf(stderr,"Philosopher %d takes left fork\n", self.position);
        sem_wait(&self.forks[(self.position + 1) % self.count]);
        fprintf(stderr,"Philosopher %d takes right fork\n", self.position);
        sleep(1);
        eat(self.position);
        fprintf(stderr,"Philosopher %d puts back left fork\n", self.position);
        sem_post(&self.forks[self.position]);
        fprintf(stderr,"Philosopher %d puts back right fork\n", self.position);
        sem_post(&self.forks[(self.position + 1) % self.count]);
        sem_post(self.waiter);
        sleep(1);
    }

    think(self.position);
    return NULL;
}

void think(int position)
{
    fprintf(stderr,"Philosopher %d thinking...\n", position);
}

void eat(int position)
{
    fprintf(stderr,"Philosopher %d eating...\n", position);
}
