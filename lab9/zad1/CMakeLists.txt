cmake_minimum_required(VERSION 3.4)
project(zad1)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11 -Wall -pedantic -Wextra")

set(SOURCE_FILES main.c)
add_executable(zad1 ${SOURCE_FILES})

target_link_libraries(zad1 pthread)