//
// Created by ry on 3/7/16.
//

#include <dlfcn.h>
#include <stdio.h>
#include "test_util.h"
#include "../linkedlist/Contact.h"
#include "../linkedlist/LinkedList.h"

typedef int (*print)(Contact_t*);
typedef int (*create)(Contact_t**, const char*, const char*,
                      int, int, int,
                      const char*, const char*, const char*);
typedef void (*destroy)(Contact_t*);

typedef int (*list_create)(LinkedList_t**);
typedef int (*list_sort)(LinkedList_t*);
typedef void (*list_destroy)(LinkedList_t*);
typedef int (*push)(LinkedList_t*, Contact_t*);

typedef Contact_t* (*get)(LinkedList_t*);
typedef Node_t* (*front)(LinkedList_t*);
typedef Node_t* (*next)(Node_t*);

int main(int argc, char* argv[] ) {

    if(argc < 2) {
        printf("Usage: %s [library_path]\n", argv[0]);
        return -1;
    }

    void* handle = dlopen(argv[1], RTLD_LAZY);
    if(!handle) {
        fprintf(stderr, "Failed to open library %s\n", argv[1]);
        return -1;
    }

    create Contact_create = (create)dlsym(handle, "Contact_create");
    print Contact_print = (print)dlsym(handle, "Contact_print");
    destroy Contact_destroy = (destroy)dlsym(handle, "Contact_destroy");
    list_create LinkedList_create = (list_create)dlsym(handle, "LinkedList_create");
    list_destroy LinkedList_destroy = (list_destroy)dlsym(handle, "LinkedList_destroy");
    list_sort LinkedList_sort = (list_sort)dlsym(handle, "LinkedList_sort");
    push LinkedList_push_back = (push)dlsym(handle, "LinkedList_push_back");

    front LinkedList_front = (front)dlsym(handle, "LinkedList_front");
    get Node_get = (get)dlsym(handle, "Node_get");
    next Node_next = (next)dlsym(handle,"Node_next");

    if(!Contact_create || !Contact_print || !Contact_destroy || !LinkedList_create || !LinkedList_destroy || !LinkedList_push_back || !LinkedList_sort
            || !LinkedList_front || !Node_get || !Node_next)
    {
        puts("Failed to load function\n");
        exit(-1);
    }
    int size = 10;
    LinkedList_t* list;
    LinkedList_create(&list);

    breakpoint();

    while(size>0) {
        --size;

        Contact_t* ptr = NULL;
        Contact_create(&ptr, "Ryszard", "Sikora", 1995, 11, 1, "ryszard.sikora7@pj.pl", "384-324-322", "Krakow");

        LinkedList_push_back(list, ptr);

        Contact_t* pt = NULL;
        Contact_create(&pt, "Rys", "Si", 1995, 11, 1, "ryszard.sikora7@pj.pl", "384-324-322", "Krakow");

        LinkedList_push_back(list, pt);
    }

    breakpoint();

    LinkedList_sort(list);

    breakpoint();

    Node_t* iter = LinkedList_front(list);
    while(iter) {
        printf(" {");
        Contact_print(Node_get(iter));
        printf("}");
        iter = Node_next(iter);

    }

    breakpoint();

    LinkedList_destroy(list);
    dlclose(handle);
    return 0;

}