//
// Created by ry on 3/8/16.
//

#include "test_util.h"

static void print(double real, double real_f, double real_l,
                  double usr, double usr_f, double usr_l,
                  double sys, double sys_f, double sys_l) {
    printf("\n");
    printf("type\tcurr\t\tfirst\t\tlast\n");
    printf("real\t%lf\t%lf\t%lf\n",real, real-real_f, real-real_l);
    printf("usr \t%lf\t%lf\t%lf\n", usr, usr-usr_f, usr-usr_l);
    printf("sys \t%lf\t%lf\t%lf\n", sys, sys-sys_f, sys-sys_l);
    printf("\n");
}

void breakpoint()
{
    static bool first;
    static double real, usr, sys;
    static double real_first, usr_first, sys_first;
    static double real_last, usr_last, sys_last;

    struct rusage rus;
    clock_t clk;

    getrusage(RUSAGE_SELF, &rus);
    clk = clock();

    real_last = real;
    usr_last = usr;
    sys_last = sys;

    real = clk / (double)CLOCKS_PER_SEC;
    usr = rus.ru_utime.tv_sec + rus.ru_utime.tv_usec / (double)10e6;
    sys = rus.ru_stime.tv_sec + rus.ru_stime.tv_usec / (double)10e6;

    if(!first) {
        real_first = real;
        usr_first = usr;
        sys_first = sys;
        first = true;
    }

    print(real, real_first, real_last,
            usr, usr_first, usr_last,
            sys, sys_first, sys_last);



}

