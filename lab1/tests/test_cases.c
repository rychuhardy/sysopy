//
// Created by ry on 3/7/16.
//

#include "test_cases.h"

int fill_list(LinkedList_t *list, unsigned size) {
    if(list) {
        while(size>0) {
            --size;

            Contact_t* ptr = NULL;
            Contact_create(&ptr, "Ryszard", "Sikora", 1995, 11, 1, "ryszard.sikora7@pj.pl", "384-324-322", "Krakow");

            LinkedList_push_back(list, ptr);

            Contact_t* pt = NULL;
            Contact_create(&pt, "Rys", "Si", 1995, 11, 1, "ryszard.sikora7@pj.pl", "384-324-322", "Krakow");

            LinkedList_push_back(list, pt);
        }

        return 0;
    }
    return -1;
}

int sort_list(LinkedList_t *list) {
    if(list) {
        LinkedList_sort(list);
        return 0;
    }
    return -1;
}

int remove_from_list(LinkedList_t *list, unsigned number) {
    if(list) {
        while (LinkedList_size(list) && number>0) {
            --number;
            LinkedList_pop_front(list);
        }
        return 0;
    }
    return -1;
}

int print_list(LinkedList_t *list) {
    Node_t* iter = LinkedList_front(list);
    while(iter) {
        printf(" {");
        Contact_print(Node_get(iter));
        printf("}");
        iter = Node_next(iter);

    }
    return 0;
}
