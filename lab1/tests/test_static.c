//
// Created by ry on 3/7/16.
//
#include "../linkedlist/LinkedList.h"
#include "test_cases.h"
#include "test_util.h"

int main() {

    LinkedList_t* list;
    LinkedList_create(&list);

    breakpoint();

    fill_list(list, 1000);

    breakpoint();

    sort_list(list);

    breakpoint();

    print_list(list);

    breakpoint();

    LinkedList_destroy(list);

    return 0;
}