//
// Created by ry on 3/8/16.
//

#ifndef LAB1_TEST_CASES_H
#define LAB1_TEST_CASES_H

#include "../linkedlist/LinkedList.h"

int fill_list(LinkedList_t* list, unsigned size);

int sort_list(LinkedList_t* list);

int remove_from_list(LinkedList_t* list, unsigned number);

int print_list(LinkedList_t* list);
#endif //LAB1_TEST_CASES_H
