//
// Created by ry on 3/1/16.
//


#include "Contact.h"

struct Contact {
    char* firstname;
    char* lastname;
    time_t birthdate;
    char* email;
    char* phone;
    char* address;
};

static bool is_valid_date(int year, int month, int day) {

    static int daysinmonth[12]={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//    Leap year
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
        daysinmonth[1] = 29;
    }
    else {
        daysinmonth[1]=28;
    }

    if (month<13)
    {
        if( day <= daysinmonth[month-1] )
            return true;
    }
    return false;

}


const char *Contact_get_firstname(Contact_t *contact) {
    if(contact) {
        return contact->firstname;
    }
    return NULL;
}

const char *Contact_get_lastname(Contact_t *contact) {
    if(contact) {
        return contact->lastname;
    }
    return NULL;
}

time_t Contact_get_birthdate(Contact_t *contact) {
    if(contact) {
        return contact->birthdate;
    }
    return NULL_PTR;
}

const char *Contact_get_email(Contact_t *contact) {
    if(contact) {
        return contact->email;
    }
    return NULL;
}

const char *Contact_get_phone(Contact_t *contact) {
    if(contact) {
        return contact->phone;
    }
    return NULL;
}

const char *Contact_get_address(Contact_t *contact) {
    if(contact) {
        return contact->address;
    }
    return NULL;
}

int Contact_create(Contact_t ** contact, const char *firstname, const char *lastname, int year, int month, int day,
                   const char *email, const char *phone, const char *address) {

    *contact = (Contact_t *)malloc(sizeof(Contact_t));

    if(!*contact) {
        fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
        return BAD_ALLOC;
    }

//    This is probably unnecessary. EDIT: Actually, it is.
    (*contact)->firstname=NULL;
    (*contact)->lastname=NULL;
    (*contact)->email=NULL;
    (*contact)->phone=NULL;
    (*contact)->address=NULL;

    if(Contact_set_firstname(*contact, firstname) != BAD_ALLOC
        && Contact_set_lastname(*contact, lastname) != BAD_ALLOC
        && Contact_set_birthdate(*contact, year, month, day) != BAD_ALLOC
        && Contact_set_email(*contact, email) != BAD_ALLOC
        && Contact_set_phone(*contact, phone) != BAD_ALLOC
        && Contact_set_address(*contact, address) !=BAD_ALLOC ) {
        return 0;
    }

    Contact_destroy(*contact);
    return BAD_ALLOC;

}


//Contact_t* Contact_create(const char *firstname, const char *lastname, int year, int month, int day,
//                   const char *email, const char *phone, const char *address) {
//
//    Contact_t* contact;
//    contact = (Contact_t *)malloc(sizeof(Contact_t));
//
//    if(!contact) {
//        fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
//        return NULL;
//    }
//
////    This is probably unnecessary. EDIT: Actually, it is.
//    (contact)->firstname=NULL;
//    (contact)->lastname=NULL;
//    (contact)->email=NULL;
//    (contact)->phone=NULL;
//    (contact)->address=NULL;
//
//    if(Contact_set_firstname(contact, firstname) != BAD_ALLOC
//       && Contact_set_lastname(contact, lastname) != BAD_ALLOC
//       && Contact_set_birthdate(contact, year, month, day) != BAD_ALLOC
//       && Contact_set_email(contact, email) != BAD_ALLOC
//       && Contact_set_phone(contact, phone) != BAD_ALLOC
//       && Contact_set_address(contact, address) !=BAD_ALLOC ) {
//        return contact;
//    }
//
//    Contact_destroy(contact);
//    return NULL;
//}


int Contact_set_firstname(Contact_t *contact, const char *firstname) {
    if(contact && firstname) {
        size_t len = strlen(firstname);
//    Additional space for terminating character.
        len+=1;

        contact->firstname = (char*)realloc(contact->firstname, sizeof(char)*len);

        if(!contact->firstname) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }

        strcpy(contact->firstname, firstname);
        return 0;
    }
    return NULL_PTR;

}

int Contact_set_lastname(Contact_t *contact, const char *lastname) {

    if(contact && lastname) {
        size_t len = strlen(lastname);
//    Additional space for terminating character.
        len+=1;
        contact->lastname = (char*)realloc(contact->lastname, sizeof(char)*len);

        if(!contact->lastname) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }

        strcpy(contact->lastname, lastname);
        return 0;
    }
    return NULL_PTR;

}

int Contact_set_birthdate(Contact_t *contact, int year, int month, int day) {

    if(contact) {

        if (is_valid_date(year, month, day)) {
            struct tm str_time;

            str_time.tm_year = year - 1900;
            str_time.tm_mon = month;
            str_time.tm_mday = day;
            str_time.tm_hour = 0;
            str_time.tm_min = 0;
            str_time.tm_sec = 0;
            str_time.tm_isdst = 0;

            contact->birthdate = mktime(&str_time);
            return 0;
        }
        return BAD_DATE;
    }
    return NULL_PTR;

}

int Contact_set_email(Contact_t *contact, const char *email) {

    if(contact && email) {
        size_t len = strlen(email);
//    Additional space for terminating character.
        len+=1;
        contact->email = (char*)realloc(contact->email, sizeof(char)*len);

        if(!contact->email) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }

        strcpy(contact->email, email);
        return 0;
    }
    return NULL_PTR;

}

int Contact_set_phone(Contact_t *contact, const char *phone) {

    if(contact && phone) {
        size_t len = strlen(phone);
//    Additional space for terminating character.
        len+=1;
        contact->phone = (char*)realloc(contact->phone, sizeof(char)*len);

        if(!contact->phone) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }

        strcpy(contact->phone, phone);
        return 0;
    }
    return NULL_PTR;
}

int Contact_set_address(Contact_t *contact, const char *address) {

    if(contact && address) {
        size_t len = strlen(address);
//    Additional space for terminating character.
        len+=1;
        contact->address = (char*)realloc(contact->address, sizeof(char)*len);

        if(!contact->address) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }

        strcpy(contact->address, address);
        return 0;
    }
    return NULL_PTR;

}

void Contact_destroy(Contact_t *contact) {

    if(contact) {
        free(contact->firstname);
        free(contact->lastname);
        free(contact->email);
        free(contact->phone);
        free(contact->address);
    }
    free(contact);
    contact = NULL;

}

int Contact_compare(const Contact_t *c1, const Contact_t *c2) {

    if((c1) && (c2) && (c1)->lastname && (c2)->lastname) {
        int lastname = strcmp((c1)->lastname, (c2)->lastname);

        if (!lastname && (c1)->firstname && (c2)) {
            int firstname = strcmp((c1)->firstname, (c2)->firstname);
            if (!firstname && (c1)->phone && (c2)->phone) {
                int phone = strcmp((c1)->phone, (c2)->phone);
                if (!phone && (c1)->address && (c2)->address) {
                    return strcmp((c1)->address, (c2)->address);
                }
                return phone;
            }
            return firstname;
        }
        return lastname;
    }
    return NULL_PTR;

}


int Contact_print(Contact_t * c) {

    if(c) {
        printf("Contact: \n"
                       "Firstname: %s\n"
                       "Lastname: %s\n", c->firstname, c->lastname);

        printf("Birthdate: ");
        printf(ctime(&c->birthdate));

        printf("Email: %s\n"
                       "Phone: %s\n"
                       "Address: %s\n", c->email, c->phone, c->address);
    }
    return NULL_PTR;
}
