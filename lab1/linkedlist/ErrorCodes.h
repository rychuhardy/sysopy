//
// Created by ry on 3/5/16.
//

#ifndef LAB1_ERRORCODES_H
#define LAB1_ERRORCODES_H

enum LIST_ERROR {
    BAD_ALLOC = -1,
    NULL_PTR = -2,
    CORRUPTED_LIST = -3,
    BAD_DATE = -4,
    RANGE_ERROR = -5,
    NOT_FOUND = -6
};

#endif //LAB1_ERRORCODES_H
