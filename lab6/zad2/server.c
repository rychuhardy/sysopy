#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/signal.h>
#include <mqueue.h>
#include <time.h>
#include <string.h>

#define MAX_CLIENTS 512
#define ID_BUF 1024

/*
 * MESSAGE TYPES:
 * 1 assign me new id
 * 2 ready to work
 * 3 result
 */

struct msg_buf {
    unsigned type;
    char msg[ID_BUF];
};

const char* server_name;

void handler(int signum __attribute__ ((unused))) {

    mq_unlink(server_name);

    signal(SIGINT, SIG_DFL);
    raise(SIGINT);

}

int main(int argc, char* argv[]) {

    srand(time(0));

    if(argc < 2 ) {
        fprintf(stderr, "Usage %s [server_name]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    struct mq_attr attr;
    attr.mq_msgsize = ID_BUF;
    attr.mq_maxmsg = 10;
    attr.mq_curmsgs = 0;
    attr.mq_flags = 0;

    mqd_t msgid;
    int msgflg = O_RDONLY | O_CREAT;
    if((msgid = mq_open(argv[1], msgflg, 0666, &attr)) == -1) {
        perror("mq_open");
        exit(EXIT_FAILURE);
    }

    server_name = argv[1];
    int clients[MAX_CLIENTS];
    int client_index = 1;

    signal(SIGINT, handler);

    while(true) {

        char buf[ID_BUF];
        if(mq_receive(msgid, buf, ID_BUF, NULL)==-1)
            perror("mq_receive");

        if(buf[0] == '1') {

            mqd_t mqd = mq_open(buf+1, O_WRONLY);

            clients[client_index] = mqd;

            fprintf(stderr, "Received message from %s mq_open ID: %d assigning private id: %d \n\n",buf+1, clients[client_index], client_index);

            sprintf(buf, "%d", client_index);

            int to_send = clients[client_index];

            if(mq_send(to_send, buf, strlen(buf)+1, 1)==-1)
                perror("mq_send");

            ++client_index;
        }
        else if(buf[0] == '2') {

            int id = atoi(buf+1);
            fprintf(stderr, "Received request for number from id %d\n", id);

            unsigned num = rand()%(1000000)+1;
            sprintf(buf, "%u", num);

            fprintf(stderr, "Sending number %s to %d\n\n", buf, (clients[id]));

            mq_send(clients[id], buf, strlen(buf)+1, 1);

        }
        else if(buf[0] == '3') {

            int id, number;
            int prime;

            sscanf(buf+1, "%d %d %d", &id, &number, &prime);

            fprintf(stderr, "Received answer from %d with number %d which is %d\n\n", id, number, prime);

        }


    }

    return 0;
}