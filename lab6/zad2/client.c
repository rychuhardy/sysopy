//
// Created by ry on 4/17/16.
//

#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <mqueue.h>

#define ID_BUF 32
#define MAX_CLIENTS 512

/*
 * MESSAGE TYPES:
 * 1 assign me new id
 * 2 ready to work
 * 3 result
 */

bool is_prime(unsigned n) {

    if(n < 2) {
        return false;
    }
    if(n==2 || n==3) {
        return true;
    }
    for(unsigned i=2; i*i<=n; ++i) {
        if(n%i==0) {
            return false;
        }
    }
    return true;

}

const char* private_name;

void handler(int signum __attribute__ ((unused))) {

    mq_unlink(private_name);

    signal(SIGINT, SIG_DFL);
    raise(SIGINT);

}

int main(int argc, char* argv[]) {

    if(argc < 3 ) {
        fprintf(stderr, "Usage %s [server_name] [private_name]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    private_name = argv[2];
    int oflag = O_WRONLY;
    int private_oflag = O_CREAT | O_RDONLY;
    mqd_t msgid;
    mqd_t private_msgid;

    struct mq_attr attr;
    attr.mq_msgsize = ID_BUF;
    attr.mq_maxmsg = 10;
    attr.mq_curmsgs = 0;
    attr.mq_flags = 0;

    if((msgid = mq_open(argv[1], oflag)) == -1) {
        perror("mq_open");
        exit(EXIT_FAILURE);
    }

    if((private_msgid = mq_open(argv[2], private_oflag, 0666, &attr))==-1) {
        perror("mq_open");
        exit(EXIT_FAILURE);
    }


//    fprintf(stderr, "MSGID: %d MSGID_PRIVATE: %d\n", msgid, private_msgid);

    char buf[ID_BUF];
    buf[0] = '1';
    strncpy(buf+1, argv[2], ID_BUF-1);

    if(mq_send(msgid, buf, strlen(buf)+1, 1)==-1) {
        perror("mq_send");
    }

    fprintf(stderr, "SENT REQUEST FOR REG %s\n", buf);

    if(mq_receive(private_msgid, buf, sizeof(buf), NULL)==-1) {
        perror("mq_receive");
    }

    size_t private_id = atoi(buf);


    fprintf(stderr, "private id: %zu\n", private_id);
    char prv_id[ID_BUF];
    sprintf(prv_id, "%zu", private_id);

    signal(SIGINT, handler);

    while(true) {

        buf[0] = '2';
        strcpy(buf+1, prv_id);

        if(mq_send(msgid, buf, strlen(buf)+1, 1)==-1)
            perror("mq_send");

        if(mq_receive(private_msgid, buf, ID_BUF, NULL)==-1) {
            perror("mq_receive");
        }

        int number = atoi(buf);

        if(number) {
            bool res = is_prime(number);
            fprintf(stderr, "Received number %d\n", number);

            buf[0] = '3';
            sprintf(buf+1, "%zu %d %d", private_id, number, res);

            fprintf(stderr, "Sending result %s\n", buf+1);

            if(mq_send(msgid, buf, strlen(buf)+1, 1)==-1)
                perror("mq_send");
        }
        else {
            fprintf(stderr, "Received invalid number %s\n", buf);
            raise(SIGINT);
        }

        sleep(1);
    }

    return 0;
}