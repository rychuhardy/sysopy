//
// Created by ry on 4/17/16.
//

#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/signal.h>
#include <unistd.h>

#define ID_BUF 16

/*
 * MESSAGE TYPES:
 * 1 assign me new id
 * 2 ready to work
 * 3 result
 */

struct msg_buf {
    unsigned type;
    char msg[ID_BUF];
};

bool is_prime(unsigned n) {

    if(n < 2) {
        return false;
    }
    if(n==2 || n==3) {
        return true;
    }
    for(unsigned i=2; i*i<=n; ++i) {
        if(n%i==0) {
            return false;
        }
    }
    return true;

}

int private_msgid;

void handler(int signum __attribute__ ((unused))) {

    msgctl(private_msgid, IPC_RMID, 0);

    signal(SIGINT, SIG_DFL);
    raise(SIGINT);

}

int main(int argc, char* argv[]) {

    if(argc < 3 ) {
        fprintf(stderr, "Usage %s [path] [proj_id]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int proj_id = atoi(argv[2]);

    if(!proj_id) {
        fprintf(stderr, "Invalid proj_id %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "path: %s proj_id:%d\n", argv[1], proj_id);

    int msgflg = 0644 | IPC_CREAT;
    int msgid;
    key_t key = ftok(argv[1], proj_id);

    if((msgid = msgget(key, msgflg)) == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "Server msgid: %d key: %d\n\n\n", msgid, key);

    private_msgid = msgget(IPC_PRIVATE, msgflg);
    fprintf(stderr, "private msgid: %d\n", private_msgid);

    struct msg_buf id_buf;
    id_buf.type = 1;
    sprintf(id_buf.msg, "%d", private_msgid);

    msgsnd(msgid, &id_buf, sizeof(id_buf), 0);

    msgrcv(private_msgid, &id_buf, sizeof(id_buf), 0, 0);
    size_t private_id = atoi(id_buf.msg);


    fprintf(stderr, "private id: %zu\n", private_id);
    char prv_id[ID_BUF];
    sprintf(prv_id, "%zu", private_id);

    signal(SIGINT, handler);

    while(true) {

        id_buf.type = 2;
        strcpy(id_buf.msg, prv_id);
        msgsnd(msgid, &id_buf, sizeof(id_buf), 0);

        msgrcv(private_msgid, &id_buf, sizeof(id_buf), 0, 0);

        int number = atoi(id_buf.msg);

        if(number) {
            bool res = is_prime(number);
            fprintf(stderr, "Received number %d\n", number);

            sprintf(id_buf.msg, "%zu %d %d", private_id, number, res);

            id_buf.type=3;

            fprintf(stderr, "Sending result %s\n", id_buf.msg);

            msgsnd(msgid, &id_buf, sizeof(id_buf), 0);
        }
        else {
            fprintf(stderr, "Received invalid number %s\n", id_buf.msg);
            raise(SIGINT);
        }

        sleep(1);
    }


    return 0;
}