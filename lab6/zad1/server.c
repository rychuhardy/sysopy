#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/signal.h>

#define MAX_CLIENTS 512
#define ID_BUF 16

/*
 * MESSAGE TYPES:
 * 1 assign me new id
 * 2 ready to work
 * 3 result
 */

struct msg_buf {
    unsigned type;
    char msg[ID_BUF];
};

int msgid;

void handler(int signum __attribute__ ((unused))) {

    msgctl(msgid, IPC_RMID, 0);

    signal(SIGINT, SIG_DFL);
    raise(SIGINT);

}

int main(int argc, char* argv[]) {

    if(argc < 3 ) {
        fprintf(stderr, "Usage %s [path] [proj_id]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int proj_id = atoi(argv[2]);

    if(!proj_id) {
        fprintf(stderr, "Invalid proj_id %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "path: %s proj_id:%d\n", argv[1], proj_id);

    key_t key = ftok(argv[1], proj_id);

    int msgflg = 0644 | IPC_CREAT;
    if((msgid = msgget(key, msgflg)) == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "Server msgid: %d key: %d\n\n\n", msgid, key);

    int clients[MAX_CLIENTS];
    int client_index = 1;

    signal(SIGINT, handler);

    while(true) {

        struct msg_buf id_buf;
        msgrcv(msgid, &id_buf, sizeof(id_buf), 0, 0);

        if(id_buf.type == 1) {

            clients[client_index] = atoi(id_buf.msg);

            fprintf(stderr, "Received ipc ID: %d assigning private id: %d \n\n", clients[client_index], client_index);

            sprintf(id_buf.msg, "%d", client_index);

            int to_send = clients[client_index];

            msgsnd(to_send, &id_buf, sizeof(id_buf), 0);

            ++client_index;
        }
        else if(id_buf.type == 2) {

            int id = atoi(id_buf.msg);
            fprintf(stderr, "Received request for number from id %d\n", id);

            unsigned num = rand()%(1000000)+1;
            sprintf(id_buf.msg, "%u", num);

            fprintf(stderr, "Sending number %s to %d\n\n", id_buf.msg, (clients[id]));

            msgsnd((clients[id]), &id_buf, sizeof(id_buf), 0);

        }
        else if(id_buf.type == 3) {

            int id, number;
            int prime;

            sscanf(id_buf.msg, "%d %d %d", &id, &number, &prime);

            fprintf(stderr, "Received answer from %d with number %d which is %d\n\n", id, number, prime);

        }


    }

    return 0;
}